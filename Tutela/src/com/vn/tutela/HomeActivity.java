package com.vn.tutela;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.vn.tutela.base.BaseActivity;
import com.vn.tutela.dialogs.ValidateEmailDialog;
import com.vn.tutela.dialogs.ValidateEmailDialog.DangKyListener;
import com.vn.tutela.models.User;
import com.vn.tutela.util.Constants;
import com.vn.tutela.volley.VolleySingleton;

public class HomeActivity extends BaseActivity implements OnClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_home);
		overridePendingTransition(R.anim.trans_bottom_in,
				R.anim.trans_bottom_out);
		if (mApp.currentUser == null)
			showDialog(this, getString(R.string.login_text),
					getString(R.string.dangnhap), getString(R.string.thoat),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							showProgressDialog(true);
							onClickLogin();
						}
					}, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
							return;
						}
					});
		else
			initView();
	}

	private void initView() {
		findViewById(R.id.bt_cauchuyen).setOnClickListener(this);
		findViewById(R.id.bt_dadk).setOnClickListener(this);
		findViewById(R.id.bt_chiendich).setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.bt_dadk:
			startActivity(new Intent(HomeActivity.this, CDDaDKActivity.class));
			break;
		case R.id.bt_cauchuyen:
			startActivity(new Intent(HomeActivity.this, CauChuyenActivity.class));
			break;
		case R.id.bt_chiendich:
			startActivity(new Intent(HomeActivity.this, ChienDichActivity.class));
			break;
		default:
			break;
		}
	}

	private void onClickLogin() {
		// start Facebook Login
		if (!isNetwork()) {
			showProgressDialog(false);
			finish();
			return;
		}
		List<String> permissions = new ArrayList<String>();
		permissions.add("email");
		permissions.add("user_birthday");
		Session.openActiveSession(this, true, permissions,
				new Session.StatusCallback() {

					// callback when session changes state
					@Override
					public void call(Session session, SessionState state,
							Exception exception) {
						if (session.isOpened()) {

							// make request to the /me API
							Request.newMeRequest(session,
									new Request.GraphUserCallback() {
										// callback after Graph API response
										// with user object
										@Override
										public void onCompleted(GraphUser user,
												Response response) {
											if (user != null) {
												User us = new User();
												us.setFb_id(Long.parseLong(user
														.getId()));
												us.setFirst_name(user
														.getFirstName());
												us.setLast_name(user
														.getLastName());
												us.setEmail((String) user
														.asMap().get("email"));
												String birthday = (String) user
														.asMap().get("email");
												if (birthday != null
														&& !birthday.equals(""))
													user.setBirthday(birthday);
												Log.d("USER:: ", us.toString());
												try {
													loginServer(us);
												} catch (JSONException e) {
													e.printStackTrace();
													showProgressDialog(false);
													finish();
												}
											} else {
												showProgressDialog(false);
												finish();
												return;
											}
										}
									}).executeAsync();
						}
					}
				});
	}

	private User loginServer(final User us) throws JSONException {
		DangKyListener lis = new DangKyListener() {

			@Override
			public void DKServer(final User us) {
				Log.d("User", us.toString());
				showProgressDialog(false);
				VolleySingleton
						.getInstance()
						.getRequestQueue()
						.add(new JsonObjectRequest(
								com.android.volley.Request.Method.GET,
								Constants.API_LOGIN + "?fb_id=" + us.getFb_id()
										+ "&email="
										+ URLEncoder.encode(us.getEmail())
										+ "&first_name="
										+ URLEncoder.encode(us.getFirst_name())
										+ "&last_name="
										+ URLEncoder.encode(us.getLast_name())
										+ "&city="
										+ URLEncoder.encode(us.getCity())
										+ "&birthday="
										+ us.getBirthday(),
								null,
								new Listener<JSONObject>() {

									@Override
									public void onResponse(JSONObject data) {
										try {
											Log.d("dataJson", data.toString());
											if (!data.isNull("success")
													&& data.getBoolean("success")) {
												us.setUser_id(data
														.getJSONObject("user")
														.getInt("user_id"));
												Log.d("User Server",
														us.toString());
												mApp.currentUser = us;
												mApp.getLocalStorage()
														.insertUser(us);
												showProgressDialog(false);
												initView();
											} else {
												showProgressDialog(false);
												finish();
											}
										} catch (JSONException e) {
											showProgressDialog(false);
											finish();
											e.printStackTrace();
										}
									}
								},
								new com.android.volley.Response.ErrorListener() {

									@Override
									public void onErrorResponse(VolleyError arg0) {
										VolleyLog.d(arg0.getMessage(), null);
										showProgressDialog(false);
										finish();
									}
								}));
			}
		};
		ValidateEmailDialog valiDialog = new ValidateEmailDialog(
				HomeActivity.this, R.style.PleaseWaitDialog, us, lis);
		valiDialog.show();

		return us;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
	}

	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onBackPressed() {
		if (doubleBackToExitPressedOnce) {
			finish();
			return;
		}
		this.doubleBackToExitPressedOnce = true;
		Toast.makeText(this, getString(R.string.press_back_finish),
				Toast.LENGTH_SHORT).show();
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				doubleBackToExitPressedOnce = false;
			}
		}, 2000);
	}

	@Override
	public void finish() {
		int pid = android.os.Process.myPid();
		android.os.Process.killProcess(pid);
		super.finish();
		overridePendingTransition(R.anim.trans_top_in, R.anim.trans_top_out);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
