package com.vn.tutela;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vn.tutela.base.BaseActivity;
import com.vn.tutela.models.ChienDich;
import com.vn.tutela.models.ChienDichStep;
import com.vn.tutela.models.UserCP;
import com.vn.tutela.util.Constants;
import com.vn.tutela.volley.VolleySingleton;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class DKCDActivity extends BaseActivity implements OnClickListener {

	ImageView image;
	int i;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dkcd);
		overridePendingTransition(R.anim.trans_bottom_in,
				R.anim.trans_bottom_out);
		i = getIntent().getIntExtra(Constants.EXTRA_IDMOBILE, 0);
		boolean isOnline = getIntent().getBooleanExtra(
				Constants.EXTRA_ISONLINE, false);

		image = (ImageView) findViewById(R.id.bg_dkcd);
		File imageFile = new File(Constants.FOLDER_IMAGE + "/"
				+ mApp.getListCDmobile().get(i).getImg_regis());
		if(imageFile.exists()){
			Bitmap bitmap = BitmapFactory.decodeFile(imageFile
					.getAbsolutePath());
			if (bitmap != null)
				image.setImageBitmap(bitmap);
		}else
		if (isOnline) {
			ImageRequest request = new ImageRequest(Constants.URL_IMG
					+ mApp.getListCDmobile().get(i).getImg_regis(),
					new Response.Listener() {

						@Override
						public void onResponse(Object bitmap) {
							image.setImageBitmap((Bitmap) bitmap);
							try {
								saveBitmap((Bitmap) bitmap, mApp
										.getListCDmobile().get(i)
										.getImg_regis());
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}
						}
					}, 0, 0, null, new Response.ErrorListener() {
						public void onErrorResponse(VolleyError error) {
							// story_image.setImageResource(R.drawable.image_load_error);
						}
					});
			// Access the RequestQueue through your singleton class.
			VolleySingleton.getInstance().getRequestQueue().add(request);
		}
		findViewById(R.id.bt_dkcd).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.bt_dkcd) {
			if (!isNetwork())
				return;
			showProgressDialog(true);
			try {
				regis();
			} catch (JSONException e) {
				e.printStackTrace();
				showProgressDialog(true);
			}
		}
	}

	private void regis() throws JSONException {
		JSONObject request = new JSONObject();
		request.put("user_id", mApp.currentUser.getUser_id());
		request.put("fb_id", mApp.currentUser.getUser_id());
		request.put("id_chiendich", mApp.getListCDmobile().get(i)
				.getId_chiendich());
		VolleySingleton
				.getInstance()
				.getRequestQueue()
				.add(new JsonObjectRequest(
						com.android.volley.Request.Method.GET,
						Constants.API_DANGKY
								+ "?user_id="
								+ mApp.currentUser.getUser_id()
								+ "&fb_id="
								+ mApp.currentUser.getFb_id()
								+ "&id_chiendich="
								+ mApp.getListCDmobile().get(i)
										.getId_chiendich(), request,
						new Listener<JSONObject>() {

							@Override
							public void onResponse(JSONObject data) {
								try {
									Log.d("dataJson", data.toString());
									if (!data.isNull("success")
											&& data.getBoolean("success")) {
										showToast(getString(R.string.regis_success));
										if (!data.isNull("chiendich")) {
											ChienDich cdVuaDk = insertData(data
													.getJSONObject("chiendich"));
											if(mApp.getListCDDK() == null){
												mApp.refreshCDDaDK();
											}
											mApp.getListCDDK().add(0, cdVuaDk);
											ArrayList<ChienDich> newListCD = new ArrayList<ChienDich>();
											newListCD.add(cdVuaDk);
											mApp.getLocalStorage().insertListCD(newListCD);
											//mApp.refreshCDDaDK();
											
										}
									} else {
										if (!data.isNull("message"))
											showToast(data.getString("message"));
									}
									showProgressDialog(false);
								} catch (JSONException e) {
									showProgressDialog(false);
									finish();
									e.printStackTrace();
								}
							}
						}, new com.android.volley.Response.ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError arg0) {
								VolleyLog.d(arg0.getMessage(), null);
								showProgressDialog(false);
								finish();
							}
						}));
	}

	private ChienDich insertData(JSONObject jsCD) throws JSONException {
		ChienDich cd = new ChienDich();
		Log.d("DataJSOn", "CD " + i + ": " + jsCD.toString());
		if (!jsCD.isNull("id"))
			cd.setId_dk(jsCD.getInt("id"));
		if (!jsCD.isNull("chiendich")) {
			JSONObject chiendich = jsCD.getJSONObject("chiendich");
			cd.setId(chiendich.getInt("id"));
			if (chiendich.has("create"))
				cd.setCreate(Long.parseLong(chiendich.getString("create")));
			cd.setTitle(chiendich.getString("title"));
			cd.setSort_des(chiendich.getString("sort_des"));
			cd.setDate_create(chiendich.getLong("date_create"));
			cd.setDate_end(chiendich.getLong("date_end"));
			cd.setImage(chiendich.getString("image"));
			cd.setBanner(chiendich.getString("banner"));
			if (!chiendich.isNull("step")) {
				JSONArray jsListStep = chiendich.getJSONArray("step");
				ArrayList<ChienDichStep> listStep = new ArrayList<ChienDichStep>();
				for (int j = 0; j < jsListStep.length(); j++) {
					ChienDichStep step = new ChienDichStep();
					JSONObject jsStep = jsListStep.getJSONObject(j);
					if (!jsStep.isNull("id_step"))
						step.setId_step(jsStep.getInt("id_step"));
					if (!jsStep.isNull("right"))
						step.setRight(jsStep.getString("right"));
					if (!jsStep.isNull("left"))
						step.setLeft(jsStep.getString("left"));
					if (!jsStep.isNull("step_name"))
						step.setStep_name(jsStep.getString("step_name"));
					step.setId_chiendich(cd.getId());
					listStep.add(step);
				}
				cd.setList_step(listStep);
			}
		}
		UserCP us = new UserCP();
		us.setStt(jsCD.getInt("stt"));
		us.setIsWeb(jsCD.getInt("isWeb"));
		cd.setUser_cp(us);
		return cd;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.trans_top_in, R.anim.trans_top_out);
	}

}
