package com.vn.tutela;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vn.tutela.adapter.AdapterListChiendichdk;
import com.vn.tutela.base.BaseActivity;
import com.vn.tutela.models.ChienDich;
import com.vn.tutela.models.ChienDichStep;
import com.vn.tutela.models.UserCP;
import com.vn.tutela.util.Constants;
import com.vn.tutela.volley.VolleySingleton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class CDDaDKActivity extends BaseActivity {

	ListView lvCd;
	ArrayList<ChienDich> listDkcd;
	AdapterListChiendichdk adapter;
	private boolean isOnline = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_da_dk);
		overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
		showProgressDialog(true);
		lvCd = (ListView) findViewById(R.id.lv_chiendichdadk);
		lvCd.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int p,
					long arg3) {
				Intent i = new Intent(CDDaDKActivity.this, StepCDActivity.class);
				i.putExtra(Constants.EXTRA_IDCDDK, p);
				i.putExtra(Constants.EXTRA_ISONLINE, isOnline);
				startActivity(i);
			}
		});
		if (mApp.getListCDDK() == null)
			getData();
		else
			createData(false);
	}

	private void createData(boolean isOnline) {
		if (mApp.getListCDDK() == null) {
			noData();
			return;
		}
		this.isOnline = isOnline;
		adapter = new AdapterListChiendichdk(getApplicationContext(),
				mApp.getListCDDK());
		lvCd.setAdapter(adapter);
		showProgressDialog(false);
		mApp.setFirst(false);
	}

	private void getData() {
		String request = Constants.API_CDDK + "?user_id="
				+ mApp.currentUser.getUser_id() + "&fb_id="
				+ mApp.currentUser.getFb_id();
		Log.d("isFirst", mApp.isFirst() + "");
		if (!mApp.isFirst()) {
			mApp.setListCDDK(mApp.getLocalStorage().getListCDDK());
			request = request + "&only_web=1&max_id="
					+ mApp.getLocalStorage().getMaxIDCDDK();
		}
		if (isNetwork())
			VolleySingleton
					.getInstance()
					.getRequestQueue()
					.add(new JsonObjectRequest(Request.Method.GET, request,
							null, new Listener<JSONObject>() {

								@Override
								public void onResponse(JSONObject data) {
									try {
										Log.d("dataJson", data.toString());
										listDkcd = insertData(data);
										if (mApp.getListCDDK() == null)
											mApp.setListCDDK(listDkcd);
										else {
											ArrayList<ChienDich> lsOld = mApp
													.getListCDDK();
											lsOld.addAll(0, listDkcd);
											mApp.setListCDDK(lsOld);
										}
										mApp.getLocalStorage().insertListCD(
												mApp.getListCDDK());
										createData(true);
									} catch (JSONException e) {
										showProgressDialog(false);
										e.printStackTrace();
									}
								}
							}, new Response.ErrorListener() {

								@Override
								public void onErrorResponse(VolleyError arg0) {
									showProgressDialog(false);
								}
							}));
		else
			createData(false);
	}

	private void noData() {
		showProgressDialog(false);
		showToast(getString(R.string.check_network_in_first));
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				int pid = android.os.Process.myPid();
				android.os.Process.killProcess(pid);
				finish();
			}
		}, 2000);
	}

	private void loadDatabase() {
		mApp.setListCDDK(mApp.getLocalStorage().getListCDDK());
		createData(false);
	}

	private ArrayList<ChienDich> insertData(JSONObject data)
			throws JSONException {
		if (data.has("success") && data.getBoolean("success")) {
			if (data.has("size") && data.has("list_dk")) {
				ArrayList<ChienDich> listCD = new ArrayList<ChienDich>();
				JSONArray list_dk = data.getJSONArray("list_dk");
				if (list_dk == null)
					return listCD;
				for (int i = 0; i < list_dk.length(); i++) {
					ChienDich cd = new ChienDich();
					JSONObject jsCD = list_dk.getJSONObject(i);
					Log.d("DataJSOn", "CD " + i + ": " + jsCD.toString());
					if (!jsCD.isNull("id"))
						cd.setId_dk(jsCD.getInt("id"));
					if (!jsCD.isNull("chiendich")) {
						JSONObject chiendich = jsCD.getJSONObject("chiendich");
						cd.setId(chiendich.getInt("id"));
							cd.setTitle(chiendich.getString("title"));
						if (chiendich.has("create")){
							try {
								cd.setCreate(Long.parseLong(chiendich
										.getString("create")));
							} catch (NumberFormatException e) {
								cd.setCreate(0);
							}
						}
							cd.setSort_des(chiendich.getString("sort_des"));
							cd.setDate_create(chiendich.getLong("date_create"));
							cd.setDate_end(chiendich.getLong("date_end"));
							cd.setImage(chiendich.getString("image"));
							cd.setBanner(chiendich.getString("banner"));
						if (!chiendich.isNull("step")) {
							JSONArray jsListStep = chiendich
									.getJSONArray("step");
							ArrayList<ChienDichStep> listStep = new ArrayList<ChienDichStep>();
							for (int j = 0; j < jsListStep.length(); j++) {
								ChienDichStep step = new ChienDichStep();
								JSONObject jsStep = jsListStep.getJSONObject(j);
								if (!jsStep.isNull("id_step"))
									step.setId_step(jsStep.getInt("id_step"));
								if (!jsStep.isNull("right"))
									step.setRight(jsStep.getString("right"));
								if (!jsStep.isNull("left"))
									step.setLeft(jsStep.getString("left"));
								if (!jsStep.isNull("step_name"))
									step.setStep_name(jsStep
											.getString("step_name"));
								step.setId_chiendich(cd.getId());
								listStep.add(step);
							}
							cd.setList_step(listStep);
						}
					}
					UserCP us = new UserCP();
					us.setStt(jsCD.getInt("stt"));
					us.setIsWeb(jsCD.getInt("isWeb"));
					cd.setUser_cp(us);
					listCD.add(cd);
				}
				Log.d("Return list", listCD.size() + "");
				return listCD;
			}
		}
		return new ArrayList<ChienDich>();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adapter != null)
			adapter.notifyDataSetChanged();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
	}
}
