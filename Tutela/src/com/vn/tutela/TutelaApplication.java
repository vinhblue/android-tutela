package com.vn.tutela;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vn.tutela.database.LocalStorage;
import com.vn.tutela.database.StorageUnavailableException;
import com.vn.tutela.models.CDMobile;
import com.vn.tutela.models.ChienDich;
import com.vn.tutela.models.ChienDichStep;
import com.vn.tutela.models.Story;
import com.vn.tutela.models.User;
import com.vn.tutela.models.UserCP;
import com.vn.tutela.util.Constants;
import com.vn.tutela.util.FileManager;
import com.vn.tutela.volley.VolleySingleton;

public class TutelaApplication extends Application{
	
	public static User currentUser;
	
	public static Context mAppContext;

	public static Context getAppContext() {
		return mAppContext;
	}

	public static void setAppContext(Context mAppContext) {
		TutelaApplication.mAppContext = mAppContext;
	}
	
	private LocalStorage localStorage;
	private Handler handler;
	public static SharedPreferences mPref;
	public static String URL_TEST = "http://api.tutela.vn/site/getcddk?user_id=3&fb_id=331643513659685";
	
	private ArrayList<ChienDich> listCDDK;
	private ArrayList<CDMobile> listCDmobile;
	private Story story;
	
	public Story getStory() {
		return story;
	}

	public void setStory(Story story) {
		this.story = story;
	}

	public ArrayList<ChienDich> getListCDDK() {
		return listCDDK;
	}

	public void setListCDDK(ArrayList<ChienDich> listCDDK) {
		this.listCDDK = listCDDK;
	}

	public ArrayList<CDMobile> getListCDmobile() {
		return listCDmobile;
	}

	public void setListCDmobile(ArrayList<CDMobile> listCDmobile) {
		this.listCDmobile = listCDmobile;
	}
	
	

	@Override
	public void onCreate() {
		super.onCreate();
		//File rootfd = new File(FileManager.EXTERNAL_FOLDER, Constants.FOLDER_ROOT_NAME);
		TutelaApplication.setAppContext(getApplicationContext());
		TutelaApplication.mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		handler = new Handler();
		try {
			localStorage = LocalStorage.create(this);
			TutelaApplication.currentUser = localStorage.getUser();
		} catch (StorageUnavailableException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	public void recreateLocalStorage() {
		if (localStorage == null) {
			handler.post(new Runnable() {

				@Override
				public void run() {
					try {
						localStorage = LocalStorage
								.create(TutelaApplication.this);
					} catch (StorageUnavailableException e) {
						e.printStackTrace();
					} catch (NullPointerException e) {
						e.printStackTrace();
					}
				}
			});
		}
	}
	
	public final LocalStorage getLocalStorage() {
		return localStorage;
	}

	public static LocalStorage getLocalStorage(Context context) {
		return ((TutelaApplication) context.getApplicationContext())
				.getLocalStorage();
	}
	private ArrayList<ChienDich> listDkcd;
	
	public void refreshCDDaDK(){
		String request = Constants.API_CDDK + "?user_id="
				+ currentUser.getUser_id() + "&fb_id="
				+ currentUser.getFb_id();
		if (!isFirst()) {
			setListCDDK(getLocalStorage().getListCDDK());
			request = request + "&only_web=1&max_id="
					+ getLocalStorage().getMaxIDCDDK();
		}
		if (isNetwork())
			VolleySingleton
					.getInstance()
					.getRequestQueue()
					.add(new JsonObjectRequest(Request.Method.GET, request,
							null, new Listener<JSONObject>() {

								@Override
								public void onResponse(JSONObject data) {
									try {
										Log.d("dataJson", data.toString());
										listDkcd = insertData(data);
										if (getListCDDK() == null)
											setListCDDK(listDkcd);
										else{
											ArrayList<ChienDich> lsOld = getListCDDK();
											lsOld.addAll(0, listDkcd);
											setListCDDK(lsOld);
										}
										getLocalStorage().insertListCD(
												getListCDDK());
										//createData(true);

									} catch (JSONException e) {
										e.printStackTrace();
									}
								}
							}, new Response.ErrorListener() {

								@Override
								public void onErrorResponse(VolleyError arg0) {
								}
							}));
		if(getListCDDK() != null)
			setFirst(false);
	}
	
	private ArrayList<ChienDich> insertData(JSONObject data)
			throws JSONException {
		if (data.has("success") && data.getBoolean("success")) {
			if (data.has("size") && data.has("list_dk")) {
				ArrayList<ChienDich> listCD = new ArrayList<ChienDich>();
				JSONArray list_dk = data.getJSONArray("list_dk");
				if (list_dk == null)
					return listCD;
				for (int i = 0; i < list_dk.length(); i++) {
					ChienDich cd = new ChienDich();
					JSONObject jsCD = list_dk.getJSONObject(i);
					Log.d("DataJSOn", "CD " + i + ": " + jsCD.toString());
					if (!jsCD.isNull("chiendich")) {
						JSONObject chiendich = jsCD.getJSONObject("chiendich");
						cd.setId(chiendich.getInt("id"));
						if (chiendich.has("create"))
							try {
								cd.setCreate(Long.parseLong(chiendich
										.getString("create")));
							} catch (NumberFormatException e) {
								cd.setCreate(0);
							}
							cd.setTitle(chiendich.getString("title"));
							cd.setSort_des(chiendich.getString("sort_des"));
							cd.setDate_create(chiendich.getLong("date_create"));
							cd.setDate_end(chiendich.getLong("date_end"));
							cd.setImage(chiendich.getString("image"));
							cd.setBanner(chiendich.getString("banner"));
						if (!chiendich.isNull("step")) {
							JSONArray jsListStep = chiendich
									.getJSONArray("step");
							ArrayList<ChienDichStep> listStep = new ArrayList<ChienDichStep>();
							for (int j = 0; j < jsListStep.length(); j++) {
								ChienDichStep step = new ChienDichStep();
								JSONObject jsStep = jsListStep.getJSONObject(j);
								if (!jsStep.isNull("id_step"))
									step.setId_step(jsStep.getInt("id_step"));
								if (!jsStep.isNull("right"))
									step.setRight(jsStep.getString("right"));
								if (!jsStep.isNull("left"))
									step.setLeft(jsStep.getString("left"));
								if (!jsStep.isNull("step_name"))
									step.setStep_name(jsStep
											.getString("step_name"));
								step.setId_chiendich(cd.getId());
								listStep.add(step);
							}
							cd.setList_step(listStep);
						}
					}
					UserCP us = new UserCP();
					us.setStt(jsCD.getInt("stt"));
					us.setIsWeb(jsCD.getInt("isWeb"));
					cd.setUser_cp(us);
					listCD.add(cd);
				}
				Log.d("Return list", listCD.size() + "");
				return listCD;
			}
		}
		return new ArrayList<ChienDich>();
	}
	
	protected boolean isNetwork() {
		ConnectivityManager connectivity = (ConnectivityManager) this
				.getApplicationContext().getSystemService(
						Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
		}
		Toast.makeText(this, this.getString(R.string.check_network),
				Toast.LENGTH_SHORT).show();
		return false;
	}

	public boolean isFirst() {
		return mPref.getBoolean("isFirst", true);
	}

	public void setFirst(boolean isFirst) {
		mPref.edit().putBoolean("isFirst", isFirst).commit();
	}
}
