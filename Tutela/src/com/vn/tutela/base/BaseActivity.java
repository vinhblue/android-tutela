package com.vn.tutela.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.vn.tutela.R;
import com.vn.tutela.TutelaApplication;
import com.vn.tutela.util.Constants;
import com.vn.tutela.util.WaitingDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

public class BaseActivity extends Activity {

	protected WaitingDialog progress;
	protected TutelaApplication mApp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (TutelaApplication) getApplication();
		progress = new WaitingDialog(this, R.style.PleaseWaitDialog, null);
		progress.setCancelable(false);
		// progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	}

	protected void showProgressDialog(boolean show) {
		if (show) {
			progress.show();
		} else {
			progress.dismiss();
		}
	}

	protected void showDialog(Context context, String title, String btLeft,
			String btRight, OnClickListener LeftClick,
			OnClickListener RightClick) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(title).setPositiveButton(btLeft, LeftClick)
				.setNegativeButton(btRight, RightClick);
		builder.create().show();
	}

	protected boolean isNetwork() {
		ConnectivityManager connectivity = (ConnectivityManager) mApp
				.getApplicationContext().getSystemService(
						Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
		}
		Toast.makeText(mApp, mApp.getString(R.string.check_network),
				Toast.LENGTH_SHORT).show();
		return false;
	}

	protected Uri saveBitmap(Bitmap bitmap, String name)
			throws FileNotFoundException {
		File imageFile = new File(Constants.FOLDER_IMAGE, name);
		if(!imageFile.getParentFile().exists())
			imageFile.getParentFile().mkdirs();
		FileOutputStream fileOutPutStream = null;
		try {
			fileOutPutStream = new FileOutputStream(imageFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutPutStream);

			fileOutPutStream.flush();
			fileOutPutStream.close();
			return Uri.parse("file://" + imageFile.getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	protected void showToast(String mess){
		Toast.makeText(mApp, mess,
				Toast.LENGTH_LONG).show();
	}
}
