package com.vn.tutela.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.text.Editable;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.vn.tutela.R;
import com.vn.tutela.models.User;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateEmailDialog extends Dialog {
	private DangKyListener dkListener;
	private EditText ed_birth;
	private EditText ed_city;
	private EditText ed_email_new;
	private Activity parentActivity;
	private User user;

	public ValidateEmailDialog(Context paramContext) {
		super(paramContext);
	}

	public ValidateEmailDialog(Context paramContext, int paramInt,
			User paramUser, DangKyListener paramDangKyListener) {
		super(paramContext, paramInt);
		setContentView(R.layout.validate_email_layout);
		this.parentActivity = ((Activity) paramContext);
		setCancelable(false);
		this.user = paramUser;
		this.dkListener = paramDangKyListener;
		initViews();
	}

	private boolean checkData() {
		if (isEmailValid(this.ed_email_new.getText().toString())) {
			this.user.setEmail(this.ed_email_new.getText().toString());
		} else {
			this.ed_email_new.setError(getContext().getString(R.string.not_email),
					getContext().getResources().getDrawable(R.drawable.indicator_input_error));
			this.ed_email_new.requestFocus();
			return false;
		}
		if (this.ed_city.getText().toString().equals("")) {
			this.ed_city.setError(getContext().getString(R.string.not_city),
					getContext().getResources().getDrawable(R.drawable.indicator_input_error));
			this.ed_city.requestFocus();
			return false;
		}
		this.user.setCity(this.ed_city.getText().toString());
		String str = this.ed_birth.getText().toString();
		int i = 0;
		try {
			i = Integer.parseInt(str);
		} catch (NumberFormatException localNumberFormatException) {
			i = 0;
		}
		if ((i > 2010) || (i < 1960)) {
			this.ed_birth.setError(getContext().getString(R.string.not_birth),
					getContext().getResources().getDrawable(R.drawable.indicator_input_error));
			this.ed_birth.requestFocus();
			return false;
		}
		this.user.setBirthday(i);
		return true;
	}

	private void initViews() {
		this.ed_city = ((EditText) findViewById(R.id.city_info));
		this.ed_birth = ((EditText) findViewById(R.id.ed_birth));
		this.ed_email_new = ((EditText) findViewById(R.id.email_new));
		findViewById(R.id.bt_dangky_email).setOnClickListener(new View.OnClickListener() {
			public void onClick(View paramView) {
				if (ValidateEmailDialog.this.checkData()) {
					ValidateEmailDialog.this.dkListener
							.DKServer(ValidateEmailDialog.this.user);
					ValidateEmailDialog.this.dismiss();
				}
			}
		});
		this.ed_email_new.setText(this.user.getEmail());
	}

	boolean isEmailValid(CharSequence paramCharSequence) {
		return Patterns.EMAIL_ADDRESS.matcher(paramCharSequence).matches();
	}

	public void onBackPressed() {
		if (this.parentActivity != null)
			this.parentActivity.onBackPressed();
	}

	public interface DangKyListener {
		public void DKServer(User paramUser);
	}
}