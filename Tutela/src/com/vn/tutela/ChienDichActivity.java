package com.vn.tutela;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;
import com.vn.tutela.adapter.AdapterChienDich;
import com.vn.tutela.fragments.ChienDichFragment.FragmentOnClick;
import com.vn.tutela.models.CDMobile;
import com.vn.tutela.util.Constants;
import com.vn.tutela.util.WaitingDialog;
import com.vn.tutela.volley.VolleySingleton;
public class ChienDichActivity extends FragmentActivity implements FragmentOnClick{
	
	ViewPager mPager;
    PageIndicator mIndicator;
    AdapterChienDich adapter;
    TutelaApplication mApp;
    protected WaitingDialog progress;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chien_dich);
		overridePendingTransition(R.anim.trans_bottom_in, R.anim.trans_bottom_out);
		mApp = (TutelaApplication)getApplication();
		progress = new WaitingDialog(this, R.style.PleaseWaitDialog, null);
		progress.setCancelable(false);
		showProgressDialog(true);
		if(mApp.getListCDmobile() == null){
			getData();
		}else
			createData(false);
	}
	
	private void initViews(){
		mPager = (ViewPager)findViewById(R.id.pager);
		mPager.setAdapter(adapter);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
        
	}
	
	protected void showProgressDialog(boolean show) {
		if (show) {
			progress.show();
		} else {
			progress.dismiss();
		}
	}
	
	private void createData(boolean isOnline){
		adapter = new AdapterChienDich(getSupportFragmentManager(), mApp.getListCDmobile(), this, isOnline);
		initViews();
		showProgressDialog(false);
	}

	private void getData(){
		if (isNetwork())
			VolleySingleton
					.getInstance()
					.getRequestQueue()
					.add(new JsonObjectRequest(Request.Method.GET,
							Constants.API_CDMOBILE + "?user_id=" + mApp.currentUser.getUser_id() + "&fb_id=" + mApp.currentUser.getFb_id(), null,
							new Listener<JSONObject>() {

								@Override
								public void onResponse(JSONObject data) {
									try {
										Log.d("dataJson", data.toString());
										ArrayList<CDMobile> s = insertData(data);
										mApp.setListCDmobile(s);
										mApp.getLocalStorage().insertListCDMobile(s);
										createData(true);
									} catch (JSONException e) {
										showProgressDialog(false);
										e.printStackTrace();
									}
								}
							}, new Response.ErrorListener() {

								@Override
								public void onErrorResponse(VolleyError arg0) {
									showProgressDialog(false);
								}
							}));
		else
			loadDatabase();
	}
	
	private ArrayList<CDMobile> insertData(JSONObject data) throws JSONException{
		if (data.has("success") && data.getBoolean("success")) {
			if (data.has("size") && data.has("list_cd")) {
				ArrayList<CDMobile> listCD = new ArrayList<CDMobile>();
				JSONArray list_cd = data.getJSONArray("list_cd");
				if (list_cd == null)
					return listCD;
				for (int i = 0; i < list_cd.length(); i++) {
					CDMobile cd = new CDMobile();
					JSONObject jsCD = list_cd.getJSONObject(i);
					Log.d("DataJSOn", "CD " + i + ": " + jsCD.toString());
					if(!jsCD.isNull("id_cdm"))
						cd.setId_cdm(jsCD.getInt("id_cdm"));
					if(!jsCD.isNull("id_chiendich"))
						cd.setId_chiendich(jsCD.getInt("id_chiendich"));
					if(!jsCD.isNull("img_adv"))
						cd.setImg_adv(jsCD.getString("img_adv"));
					if(!jsCD.isNull("img_regis"))
						cd.setImg_regis(jsCD.getString("img_regis"));
					if(!jsCD.isNull("img_list"))
						cd.setImg_list(jsCD.getString("img_list"));
					listCD.add(cd);
				}
				Log.d("Return list", listCD.size() + "");
				return listCD;
			}
		}
		return new ArrayList<CDMobile>();
	}
	
	private void loadDatabase(){
		mApp.setListCDmobile(mApp.getLocalStorage().getListCDMobile());
		createData(false);
	}
	
	@Override
	public void onClick(int id,boolean isOnline) {
		Intent i = new Intent(ChienDichActivity.this,DKCDActivity.class);
		i.putExtra(Constants.EXTRA_IDMOBILE, id);
		i.putExtra(Constants.EXTRA_ISONLINE, isOnline);
		startActivity(i);
	}

	protected boolean isNetwork() {
		ConnectivityManager connectivity = (ConnectivityManager) mApp
				.getApplicationContext().getSystemService(
						Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
		}
		return false;
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.trans_top_in, R.anim.trans_top_out);
	}
	
}

