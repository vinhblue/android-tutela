package com.vn.tutela;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.vn.tutela.base.BaseActivity;
import com.vn.tutela.models.ChienDich;
import com.vn.tutela.util.BitmapUtil;
import com.vn.tutela.util.Constants;
import com.vn.tutela.util.CustomMultiPartEntity;
import com.vn.tutela.util.NetworkUtil;

public class UploadActivity extends BaseActivity implements
		View.OnClickListener {
	EditText etContent;
	String filePath = "";
	int idCDDK;
	ImageView imageUp;
	UploadAsyncTask uploadA = null;

	private void initViews() {
		findViewById(R.id.bt_huy).setOnClickListener(this);
		findViewById(R.id.bt_upload).setOnClickListener(this);
		this.imageUp = ((ImageView) findViewById(2131034210));
		this.etContent = ((EditText) findViewById(2131034211));
		Bitmap localBitmap = BitmapUtil.scaleBitmap((new File(this.filePath)
				.getAbsolutePath()),200,200);
		if (localBitmap != null)
			this.imageUp.setImageBitmap(localBitmap);
	}
	
	

	/**
	 * Save bitmap to File
	 * 
	 * @param bitmap
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException
	 */
	private Uri saveBitmapUpload(Bitmap bitmap, String filePath)
			throws FileNotFoundException {

		File folder = new File(Constants.FOLDER_IMG_UPLOAD);
		if (!folder.exists())
			folder.mkdirs();
		File imageFile = new File(Constants.FOLDER_IMG_UPLOAD, filePath);
		FileOutputStream fileOutPutStream = null;
		try {
			fileOutPutStream = new FileOutputStream(imageFile);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutPutStream);

			fileOutPutStream.flush();
			fileOutPutStream.close();
			return Uri.parse("file://" + imageFile.getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// public void onBackPressed() {
	// new File(Constants.FOLDER_IMG_CACHE + File.separator
	// + "tutela_take_photo.png").deleteOnExit();
	// overridePendingTransition(R.anim.trans_top_in, R.anim.trans_top_out);
	// super.onBackPressed();
	//
	// }

	public void onClick(View paramView) {
		switch (paramView.getId()) {
		case R.id.bt_huy:
			onBackPressed();
			return;
		case R.id.bt_upload:
			uploadA = new UploadAsyncTask();
			uploadA.execute(getIntent().getStringExtra("FILE_PATH"));
			break;
		default:
			break;
		}
	}

	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.activity_upload);
		overridePendingTransition(R.anim.trans_bottom_in,
				R.anim.trans_bottom_out);
		this.idCDDK = getIntent().getIntExtra("ID_CDDK", 0);
		this.filePath = getIntent().getStringExtra("FILE_PATH");
		initViews();
	}

	private class UploadAsyncTask extends AsyncTask<String, Integer, String> {
		private String filePathParam;
		ProgressDialog pd;
		long totalSize;
		private boolean isAn = false;

		@Override
		protected String doInBackground(String[] paramArrayOfString) {
			this.filePathParam = paramArrayOfString[0];
			Log.d("FilePath", this.filePathParam);
			CustomMultiPartEntity localCustomMultiPartEntity = new CustomMultiPartEntity(
					new CustomMultiPartEntity.ProgressListener() {
						public void transferred(long paramLong) {
							int process = Integer
									.valueOf((int) (100.0F * ((float) paramLong / (float) UploadActivity.UploadAsyncTask.this.totalSize)));
							Log.d("Process", process + "");
							publishProgress(process);
						}
					});
			Charset localCharset = Charset.forName("UTF-8");
			try {
				Log.d("FilePath", UploadActivity.this.filePath);
				File localFile = new File(UploadActivity.this.filePath);
				boolean bool = localFile.exists();
				if ((!bool) || (localFile == null))
					return "{succes :false}";
				localCustomMultiPartEntity.addPart("image", new FileBody(
						localFile));
				localCustomMultiPartEntity.addPart("user_id", new StringBody(
						TutelaApplication.currentUser.getUser_id() + ""));
				localCustomMultiPartEntity.addPart("fb_id", new StringBody(
						TutelaApplication.currentUser.getFb_id() + ""));
				localCustomMultiPartEntity.addPart(
						"id_chiendich",
						new StringBody(((ChienDich) UploadActivity.this.mApp
								.getListCDDK().get(UploadActivity.this.idCDDK))
								.getId()
								+ ""));
				if (!UploadActivity.this.etContent.getText().toString()
						.equals(""))
					localCustomMultiPartEntity.addPart("story", new StringBody(
							UploadActivity.this.etContent.getText().toString(),
							localCharset));
				this.totalSize = localCustomMultiPartEntity.getContentLength();
				String str = "";
				str = NetworkUtil.postInfoWithFile(Constants.API_UPLOAD_ANH,
						null, localCustomMultiPartEntity, 10000);
				return str;
			} catch (Exception localException) {
			}
			return "{succes :false}";
		}

		@Override
		protected void onPostExecute(String paramString) {
			super.onPostExecute(paramString);
			Log.d("result", paramString);
			try {
				JSONObject localJSONObject = new JSONObject(paramString);
				if ((!localJSONObject.isNull("success"))
						&& (localJSONObject.getBoolean("success"))) {
					Bitmap localBitmap = BitmapFactory.decodeFile(new File(
							UploadActivity.this.filePath).getAbsolutePath());
					if (localBitmap != null)
						UploadActivity.this.saveBitmapUpload(
								BitmapUtil.getCircleBitmap(localBitmap, 169,
										Color.parseColor("#30ffffff")),
								(UploadActivity.this.mApp.getListCDDK()
										.get(UploadActivity.this.idCDDK))
										.getId()
										+ ".png");
					(UploadActivity.this.mApp.getListCDDK()
							.get(UploadActivity.this.idCDDK)).getUser_cp()
							.setStt(0);
					UploadActivity.this.showToast(UploadActivity.this
							.getString(R.string.success_upload));
					this.pd.dismiss();
					if(!isAn)
						UploadActivity.this.onBackPressed();
				} else {
					this.pd.dismiss();
					if (!localJSONObject.isNull("message"))
						UploadActivity.this.showToast(localJSONObject
								.getString("message"));
					return;
				}
			} catch (JSONException localJSONException) {
				localJSONException.printStackTrace();
				showToast(UploadActivity.this.getString(R.string.fail_upload));
			} catch (FileNotFoundException localFileNotFoundException) {
				localFileNotFoundException.printStackTrace();
			}
		}

		@Override
		protected void onPreExecute() {
			this.pd = new ProgressDialog(UploadActivity.this,
					R.style.TransparentProgressDialog);
			this.pd.setProgressStyle(1);
			this.pd.setProgressDrawable(UploadActivity.this.getResources()
					.getDrawable(R.drawable.custom_progress));
			this.pd.setMessage(UploadActivity.this
					.getString(R.string.upload_img));
			this.pd.setButton(UploadActivity.this.getString(R.string.an),
					new OnClickListener() {

						@Override
						public void onClick(final DialogInterface arg0,
								final int arg1) {
							isAn = true;
							arg0.dismiss();
						}
					});
			pd.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					uploadA.cancel(true);
				}
			});
			this.pd.setCancelable(false);
			this.pd.show();
			super.onPreExecute();
		}

		@Override
		protected void onCancelled() {
			Log.d("Cancel", "Cancelled");
			this.pd.dismiss();
			super.onCancelled();

		}

		@Override
		protected void onProgressUpdate(Integer... process) {
			super.onProgressUpdate(process);
			this.pd.setProgress(process[0]);
			Log.d("Process inpro", process[0] + "");
		}
	}
}