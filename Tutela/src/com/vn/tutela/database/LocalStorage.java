package com.vn.tutela.database;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import com.vn.tutela.models.CDMobile;
import com.vn.tutela.models.ChienDich;
import com.vn.tutela.models.Story;
import com.vn.tutela.models.User;


public class LocalStorage {
	
	private static final String TAG = LocalStorage.class.getName();
	private static final long FIFTY_MB = 1024L * 1024L * 50L;
	
	private Context context;
	private final LocalStorageDB lsDb;
	
	private LocalStorage(Context context){
		this.context = context;
		lsDb = new LocalStorageDB(context);
		lsDb.open();
	}
	
	public int getMaxIDCDDK(){
		return lsDb.getMaxIDCDDK();
	}
	
	public ArrayList<ChienDich> getListCDDK(){
		return lsDb.getListCDDK();
	}
	public void insertListCD(ArrayList<ChienDich> data){
		lsDb.insertListCDDK(data);
	}
	
	public ArrayList<CDMobile> getListCDMobile(){
		return lsDb.getListCDMobile();
	}
	public void insertListCDMobile(ArrayList<CDMobile> data){
		lsDb.insertCDMobile(data);
	}
	
	public User getUser(){
		return lsDb.getUser();
	}
	
	public void insertUser(User us){
		lsDb.insertUser(us);
	}
	public void insertStory(Story s){
		lsDb.insertStory(s);
	}
	public Story getStory(){
		return lsDb.getStory();
	}
	
	
	
	public void removeAllData() throws StorageUnavailableException {
		Log.d(TAG, "removeAllData()");
		checkLocalStorageState();
		lsDb.removeAllData();
	}

	private void checkLocalStorageState() throws StorageUnavailableException {
		// String externalStorageState = Environment.getExternalStorageState();
		// if (!externalStorageState.equals(Environment.MEDIA_MOUNTED)) {
		// throw new
		// StorageUnavailableException("Local storage is not available to the app.",
		// externalStorageState);
		// }
	}

	private void checkIfLocalStorageIsFull() throws StorageUnavailableException {
		long freeSpaceLeft = getFreeSpace(context);
		if (freeSpaceLeft < FIFTY_MB) {
			throw new OutOfStorageSpaceException(
					"There is less than 50MB of storage space left on external storage device. So the app considers it as being full.");
		}
	}

	/**
	 * Singleton created by the application.
	 * 
	 * @throws StorageUnavailableException
	 */
	public static LocalStorage create(Context app) throws StorageUnavailableException {
		if (app.getExternalCacheDir() == null) {
			throw new StorageUnavailableException("Local storage is not available to the app.", Environment.getExternalStorageState());
		} else {
			return new LocalStorage(app);
		}
	}

	public static long getFreeSpace(Context context) throws StorageUnavailableException {
		File externalCacheDir = context.getExternalCacheDir();
		if (externalCacheDir == null) {
			throw new StorageUnavailableException("Local storage is not available to the app.", Environment.getExternalStorageState());
		} else {
			StatFs stat = new StatFs(externalCacheDir.getPath());
			long spaceAvailSize = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();
			// One binary gigabyte equals 1,073,741,824 bytes.
			// long gigaAvailable = spaceAvailSize / 1073741824;
			return spaceAvailSize;
		}
	}
}
