package com.vn.tutela.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.vn.tutela.TutelaApplication;
import com.vn.tutela.models.CDMobile;
import com.vn.tutela.models.ChienDich;
import com.vn.tutela.models.ChienDichStep;
import com.vn.tutela.models.Story;
import com.vn.tutela.models.User;
import com.vn.tutela.models.UserCP;

/**
 * @author Vinh
 *
 */
public class LocalStorageDB {
	
	public static final String TAG = LocalStorageDB.class.getName(); 
	
	private Context mContext;
	
	private static final String DATABASE_NAME = "tutela.db";
	private static final int DATABASE_VERSION = 3;
	private DBOpenHelper dbOpenHelper;
	private SQLiteDatabase database;
	
	/*
	 * Table Name
	 */
	private static final String TABLE_USER = "user";
	private static final String TABLE_MESSAGE = "story";
	private static final String TABLE_CHIENDICH = "cd";
	private static final String TABLE_CHIENDICH_DK = "cddk";
	private static final String TABLE_STEP = "step";
	private static final String TABLE_CD_MOBILE = "cdmobi";
	private static final String TABLE_STORY = "story";
	
	/**
	 * The followings are the available columns in table 'user':
	 * @var integer $user_id
	 * @var integer $fb_id
	 * @var string $first_name
	 * @var string $last_name
	 * @var string $email
	 * @var integer $role
	 * @var integer $date_reg
	 */
	
	private static final String COLUMN_ID = "user_id";
	private static final String COLUMN_FB_ID = "fb_id";
	
	
	/**
	 * The followings are the available columns in table 'cddk':
	 * @var integer $id
	 * @var integer $create
	 * @var string $title
	 * @var string $sort_des
	 * @var integer $date_create
	 * @var integer $date_end
	 * @var string $image
	 * @var string $banner
	 * @var integer $id_kind
	 * @var integer $id_time
	 * @var integer $id_type
	 * @var integer $kind_step
	 */
	private static final String COLUMN_IDCD = "id";
	private static final String COLUMN_IDCDDK = "id_dk";
	private static final String COLUMN_CREATE = "create_date";
	private static final String COLUMN_TITLE = "title";
	private static final String COLUMN_SORT_DES = "sort_des";
	private static final String COLUMN_DATE_CREATE = "date_create";
	private static final String COLUMN_DATE_END = "date_end";
	private static final String COLUMN_IMAGE = "image";
	private static final String COLUMN_BANNER = "banner";
	private static final String COLUMN_STT = "stt";
	private static final String COLUMN_ISWEB = "isweb";
	
	public ArrayList<ChienDich> getListCDDK(){
		ArrayList<ChienDich> listCD = new ArrayList<ChienDich>();
		Cursor cursor = null;
		try{
			cursor = database.query(TABLE_CHIENDICH_DK,null,null,null,null,null,null);
			if(cursor != null){
				cursor.moveToFirst();
				do{
					ChienDich cd;
					cd = getCDFromCursor(cursor);
					listCD.add(cd);
				}while(cursor.moveToNext());
				cursor.close();
			}
		}catch(Exception e){
			listCD = new ArrayList<ChienDich>();
			Log.e(TAG, e.toString());
		}
		return listCD;
	}
	
	public int getMaxIDCDDK(){
		Cursor cursor = null;
		try{
			cursor = database.query(TABLE_CHIENDICH_DK,new String[]{COLUMN_IDCDDK},null,null,null,null,COLUMN_IDCDDK + " DESC","1");
			if(cursor != null){
				cursor.moveToFirst();
				int id = cursor.getInt(cursor.getColumnIndex(COLUMN_IDCDDK));
				cursor.close();
				return id;
			}
		}catch(Exception e){
			Log.e(TAG, e.toString());
		}
		return 0;
	}
	
	private ContentValues getCVFromCD(ChienDich data){
		ContentValues ret = new ContentValues();
		ret.put(COLUMN_IDCD, data.getId());
		ret.put(COLUMN_IDCDDK, data.getId_dk());
		ret.put(COLUMN_CREATE, data.getCreate());
		ret.put(COLUMN_TITLE,data.getTitle());
		ret.put(COLUMN_SORT_DES,data.getSort_des());
		ret.put(COLUMN_DATE_CREATE,data.getDate_create());
		ret.put(COLUMN_DATE_END,data.getDate_end());
		ret.put(COLUMN_IMAGE,data.getImage());
		ret.put(COLUMN_BANNER,data.getBanner());
		ret.put(COLUMN_STT,data.getUser_cp().getStt());
		ret.put(COLUMN_ISWEB,data.getUser_cp().getIsWeb());
		return ret;
	}
	
	private ChienDich getCDFromCursor(Cursor cursor){
		ChienDich cd = new ChienDich();
		cd.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_IDCD)));
		cd.setId_dk(cursor.getInt(cursor.getColumnIndex(COLUMN_IDCDDK)));
		cd.setCreate(cursor.getLong(cursor.getColumnIndex(COLUMN_CREATE)));
		cd.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
		cd.setSort_des(cursor.getString(cursor.getColumnIndex(COLUMN_SORT_DES)));
		cd.setDate_create(cursor.getLong(cursor.getColumnIndex(COLUMN_DATE_CREATE)));
		cd.setDate_end(cursor.getLong(cursor.getColumnIndex(COLUMN_DATE_END)));
		cd.setImage(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
		cd.setBanner(cursor.getString(cursor.getColumnIndex(COLUMN_BANNER)));
		UserCP us = new UserCP();
		us.setStt(cursor.getInt(cursor.getColumnIndex(COLUMN_STT)));
		us.setIsWeb(cursor.getInt(cursor.getColumnIndex(COLUMN_ISWEB)));
		cd.setUser_cp(us);
		cd.setList_step(getListStep(cd.getId()));
		return cd;
	}
	
	public void insettCDDK(ChienDich data){

		try{
			synchronized (this) {
				database.beginTransaction();
				try{
					database.insertWithOnConflict(TABLE_CHIENDICH_DK, null, getCVFromCD(data),SQLiteDatabase.CONFLICT_REPLACE);
					database.setTransactionSuccessful();
				}finally{
					database.endTransaction();
				}
			}
		}catch(SQLException e){
			Log.e("SQLException", e.getMessage());
		}
	}
	
	/**
	 * The followings are the available columns in table 'step':
	 * @var integer $id_step
	 * @var integer $id_chiendich
	 * @var string $right
	 * @var string $left
	 * @var string $step_name
	 */
	private static final String COLUMN_IDSTEP = "id_step";
	private static final String COLUMN_IDCDSTEP = "id_chiendich";
	private static final String COLUMN_RIGHT = "right";
	private static final String COLUMN_LEFT = "left";
	private static final String COLUMN_STEP_NAME = "name";
	
	private ArrayList<ChienDichStep> getListStep(int id_cd){
		ArrayList<ChienDichStep> listStep = new ArrayList<ChienDichStep>();
		Cursor cursor = null;
		try{
			cursor = database.query(TABLE_STEP,null,COLUMN_IDCDSTEP + " = " + id_cd,null,null,null,null);
			if(cursor != null){
				cursor.moveToFirst();
				do{
					ChienDichStep step = new ChienDichStep();
					step.setId_step(cursor.getInt(cursor.getColumnIndex(COLUMN_IDSTEP)));
					step.setId_chiendich(id_cd);
					step.setRight(cursor.getString(cursor.getColumnIndex(COLUMN_RIGHT)));
					step.setLeft(cursor.getString(cursor.getColumnIndex(COLUMN_LEFT)));
					step.setStep_name(cursor.getString(cursor.getColumnIndex(COLUMN_STEP_NAME)));
					listStep.add(step);
				}while(cursor.moveToNext());
				cursor.close();
			}
		}catch(Exception e){
			listStep = new ArrayList<ChienDichStep>();
			Log.e(TAG, e.toString());
		}
		return listStep;
	}
	
	public void insertStep(ChienDichStep data){
		ContentValues ret = new ContentValues(4);
		ret.put(COLUMN_IDSTEP, data.getId_step());
		ret.put(COLUMN_IDCDSTEP, data.getId_chiendich());
		ret.put(COLUMN_RIGHT,data.getRight());
		ret.put(COLUMN_LEFT,data.getLeft());
		ret.put(COLUMN_STEP_NAME,data.getStep_name());
		try{
			synchronized (this) {
				database.beginTransaction();
				try{
					database.insertWithOnConflict(TABLE_STEP, null, ret,SQLiteDatabase.CONFLICT_REPLACE);
					database.setTransactionSuccessful();
				}finally{
					database.endTransaction();
				}
			}
		}catch(SQLException e){
			Log.e("SQLException", e.getMessage());
		}
	}
	
	/**
	 * The followings are the available columns in table 'cdmobi':
	 * @var integer $id_cdm
	 * @var integer $id_chiendich
	 * @var integer $img_adv
	 * @var integer $img_regis
	 * @var integer $img_list
	 */
	
	private static final String COLUMN_IDMOBI = "id_cdm";
	private static final String COLUMN_IDCDMOBI = "id_chiendich";
	private static final String COLUMN_IMG_ADV = "img_adv";
	private static final String COLUMN_IMG_REGIS = "img_regis";
	private static final String COLUMN_IMG_LIST = "img_list";
	
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id_story
	 * @var string $name
	 * @var string $info1
	 * @var string $info2
	 * @var string $images
	 */
	
	private static final String COLUMN_STORY_ID = "id";
	private static final String COLUMN_STORY_NAME = "name";
	private static final String COLUMN_STORY_INFO1 = "info1";
	private static final String COLUMN_STORY_INFO2 = "info2";
	private static final String COLUMN_STORY_IMAGE = "image";
	
	public void insertStory(Story us){
		removeTableData(TABLE_STORY);
		ContentValues ret = new ContentValues();
		ret.put(COLUMN_STORY_ID, us.getId_story());
		ret.put(COLUMN_STORY_NAME, us.getName());
		ret.put(COLUMN_STORY_INFO1, us.getInfo1());
		ret.put(COLUMN_STORY_INFO2, us.getInfo2());
		ret.put(COLUMN_STORY_IMAGE, us.getImages());
		try{
			synchronized (this) {
				database.beginTransaction();
				try{
					database.insertWithOnConflict(TABLE_STORY, null, ret,SQLiteDatabase.CONFLICT_REPLACE);
					database.setTransactionSuccessful();
				}finally{
					database.endTransaction();
				}
			}
		}catch(SQLException e){
			Log.e("SQLException", e.getMessage());
		}
	}
	
	public Story getStory(){
		Story us = new Story();
		Cursor cursor = null;
		try{
			cursor = database.query(TABLE_STORY,null,null,null,null,null,null);
			if(cursor != null){
				if(cursor.moveToFirst()){
					us.setId_story(cursor.getInt(cursor.getColumnIndex(COLUMN_STORY_ID)));
					us.setName(cursor.getString(cursor.getColumnIndex(COLUMN_STORY_NAME)));
					us.setInfo1(cursor.getString(cursor.getColumnIndex(COLUMN_STORY_INFO1)));
					us.setInfo2(cursor.getString(cursor.getColumnIndex(COLUMN_STORY_INFO2)));
					us.setImages(cursor.getString(cursor.getColumnIndex(COLUMN_STORY_IMAGE)));
					return us;
				}
				cursor.close();
			}
		}catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return us;
	}
	
	/**
	 * @param context 
	 * 				of application 
	 */
	/**
	 * @param context
	 */
	public LocalStorageDB(Context context){
		this.mContext = context;
		this.dbOpenHelper = new DBOpenHelper(mContext, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	
	public void insertUser(User us){
		ContentValues ret = new ContentValues(4);
		ret.put(COLUMN_ID, us.getUser_id());
		ret.put(COLUMN_FB_ID, us.getFb_id());
		try{
			synchronized (this) {
				database.beginTransaction();
				try{
					database.insertWithOnConflict(TABLE_USER, null, ret,SQLiteDatabase.CONFLICT_REPLACE);
					database.setTransactionSuccessful();
				}finally{
					database.endTransaction();
				}
			}
		}catch(SQLException e){
			Log.e("SQLException", e.getMessage());
		}
	}
	
	public User getUser(){
		User us = new User();
		Cursor cursor = null;
		try{
			cursor = database.query(TABLE_USER,null,null,null,null,null,null);
			if(cursor != null){
				if(cursor.moveToFirst()){
					us.setFb_id(cursor.getLong(cursor.getColumnIndex(COLUMN_FB_ID)));
					us.setUser_id(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
					return us;
				}
				cursor.close();
			}
		}catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return null;
	}
	
	public ArrayList<CDMobile> getListCDMobile(){
		ArrayList<CDMobile> listCDMoblie = new ArrayList<CDMobile>();
		Cursor cursor = null;
		try{
			cursor = database.query(TABLE_CD_MOBILE,null,null,null,null,null,null);
			if(cursor != null){
				cursor.moveToFirst();
				do{
					CDMobile cd;
					cd = getCDMoFromCursor(cursor);
					listCDMoblie.add(cd);
				}while(cursor.moveToNext());
				cursor.close();
			}
		}catch(Exception e){
			listCDMoblie = new ArrayList<CDMobile>();
			Log.e(TAG, e.toString());
		}
		return listCDMoblie;
	}
	
	public ContentValues getCVCDMobile(CDMobile cdm){
		ContentValues values = new ContentValues();
		values.put(COLUMN_IDMOBI, cdm.getId_cdm());
		values.put(COLUMN_IDCDMOBI, cdm.getId_chiendich());
		values.put(COLUMN_IMG_ADV, cdm.getImg_adv());
		values.put(COLUMN_IMG_REGIS, cdm.getImg_regis());
		values.put(COLUMN_IMG_LIST,cdm.getImg_list());
		return values;
	}
	
	public CDMobile getCDMoFromCursor(Cursor cursor){
		CDMobile cdm = new CDMobile();
		cdm.setId_cdm(cursor.getInt(cursor.getColumnIndex(COLUMN_IDMOBI)));
		cdm.setId_chiendich(cursor.getInt(cursor.getColumnIndex(COLUMN_IDCDMOBI)));
		cdm.setImg_adv(cursor.getString(cursor.getColumnIndex(COLUMN_IMG_ADV)));
		cdm.setImg_regis(cursor.getString(cursor.getColumnIndex(COLUMN_IMG_REGIS)));
		cdm.setImg_list(cursor.getString(cursor.getColumnIndex(COLUMN_IMG_LIST)));
		return cdm;
	}
	
	public void insertCDMobile(ArrayList<CDMobile> data){
		removeTableData(TABLE_CD_MOBILE);
		for (CDMobile cdMobile : data) {
			insertCDMobile(cdMobile);
		}
	}
	
	public void insertCDMobile(CDMobile data){
		try{
			synchronized (this) {
				database.beginTransaction();
				try{
					database.insertWithOnConflict(TABLE_CD_MOBILE, null, getCVCDMobile(data),SQLiteDatabase.CONFLICT_REPLACE);
					database.setTransactionSuccessful();
				}finally{
					database.endTransaction();
				}
			}
		}catch(SQLException e){
			Log.e("SQLException", e.getMessage());
		}
	}
	
	public void insertListCDDK(ArrayList<ChienDich> data){
//		removeTableData(TABLE_CHIENDICH_DK);
//		removeTableData(TABLE_STEP);
		for (ChienDich chienDich : data) {
			insettCDDK(chienDich);
			insertStep(chienDich.getList_step());
		}
	}
	
	public void insertStep(ArrayList<ChienDichStep> step){
		for (ChienDichStep chienDichStep : step) {
			insertStep(chienDichStep);
		}
	}
	
	public void updateUser(){
		
	}
	
	public void removeTableData(String table) {
		try {
			synchronized (this) {
				database.beginTransaction();
				try {
					database.delete(table, null, null);
					database.setTransactionSuccessful();
				} finally {
					database.endTransaction();
				}
			}
		} catch (SQLException e) {
			handleSqlException(e);
		}
	}
	
	public void removeAllData() {
		try {
			synchronized (this) {
				database.beginTransaction();
				try {
					database.delete(TABLE_CHIENDICH, null, null);
					database.delete(TABLE_CD_MOBILE, null, null);
					database.delete(TABLE_CHIENDICH_DK, null, null);
					database.delete(TABLE_STEP, null, null);
					database.setTransactionSuccessful();
				} finally {
					database.endTransaction();
				}
			}
		} catch (SQLException e) {
			handleSqlException(e);
		}
	}

	/**
	 * Open a writable instance of the database.
	 * 
	 * @throws SQLiteException
	 */
	protected void open() throws SQLiteException {
		database = dbOpenHelper.getWritableDatabase();
		System.out.println("open database = " + database);
	}

	/**
	 * Close the database.
	 */
	protected void close() {
		database.close();
		database = null;
		System.out.println("close database = " + database);
	}

	private void closeAndOpenWritable() {
		try {
			synchronized (this) {
				close();
				open();
			}
		} catch (SQLException e) {
			Log.d(getClass().getName(), "Fatal error, unable to open db!");
			e.printStackTrace();
		}
	}

	private final void handleSqlException(SQLException e) {
		Log.e(LocalStorageDB.class.getName(), "handleSqlException() :: SQL Exception detected:");
		Log.e(LocalStorageDB.class.getName(), "handleSqlException() :: Cache dir: " + mContext.getExternalCacheDir());
		Log.e(LocalStorageDB.class.getName(), "handleSqlException() :: External storage state: " + Environment.getExternalStorageState());
		try {
			Log.e(LocalStorageDB.class.getName(), "handleSqlException() :: Free space: " + LocalStorage.getFreeSpace(mContext));
		} catch (StorageUnavailableException e1) {
			e1.printStackTrace();
		}
		Log.e(LocalStorageDB.class.getName(), "handleSqlException() :: printing exception ...");
		e.printStackTrace(System.err);
		closeAndOpenWritable();
	}
	
	private class DBOpenHelper extends SQLiteOpenHelper {

		// SQL Statements to create new tables:
		private static final String CREATE_TABLE_USER = "create table " + TABLE_USER + " (" + COLUMN_ID + " integer primary key, "
				+ COLUMN_FB_ID + " integer not null);";
		private static final String CREATE_TABLE_CDMOBI = "create table " + TABLE_CD_MOBILE + " (" + 
				COLUMN_IDMOBI + " integer primary key, " +
				COLUMN_IDCDMOBI + " integer not null, " +
				COLUMN_IMG_ADV + " text, " +
				COLUMN_IMG_REGIS + " text, " +
				COLUMN_IMG_LIST + " text);";
		private static final String CREATE_TABLE_CDDK = "create table " + 
				TABLE_CHIENDICH_DK + 
				" ( " +
				COLUMN_IDCD + 
				" integer primary key, " +
				COLUMN_IDCDDK +
				" integer, " +
				COLUMN_CREATE + 
				" integer, " +
				COLUMN_TITLE + 
				" text, " +
				COLUMN_SORT_DES + 
				" text, " +
				COLUMN_DATE_CREATE + 
				" integer, " +
				COLUMN_DATE_END + 
				" integer, " +
				COLUMN_IMAGE + 
				" text, " +
				COLUMN_BANNER + 
				" text, " +
				COLUMN_STT + 
				" integer, " + 
				COLUMN_ISWEB + 
				" integer );";
				
		private static final String CREATE_TABLE_STEP = "create table " 
				+ TABLE_STEP 
				+ " ( " +
				COLUMN_IDSTEP 
				+ " integer primay key, " + 
				COLUMN_IDCDSTEP 
				+ " integer not null, " +
				COLUMN_RIGHT 
				+ " text, " +
				COLUMN_LEFT 
				+ " text, " +
				COLUMN_STEP_NAME 
				+ " text " +
				");";
		
		private static final String CREATE_TABLE_STORY = "create table " 
				+ TABLE_STORY 
				+ " ( " +
				COLUMN_STORY_ID 
				+ " integer primay key, " + 
				COLUMN_STORY_NAME 
				+ " text, " +
				COLUMN_STORY_INFO1 
				+ " text, " +
				COLUMN_STORY_INFO2 
				+ " text, " +
				COLUMN_STORY_IMAGE 
				+ " text " +
				");";

		/**
		 * Default constructor.
		 * 
		 * @param context
		 *            to use to open or create the database
		 * @param name
		 *            of the database file, or null for an in-memory database
		 * @param factory
		 *            to use for creating cursor objects, or null for the
		 *            default
		 * @param version
		 *            database version number
		 */
		public DBOpenHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		/**
		 * Called when the database is first created.
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_TABLE_CDMOBI);
			db.execSQL(CREATE_TABLE_USER);
			db.execSQL(CREATE_TABLE_STORY);
			db.execSQL(CREATE_TABLE_STEP);
			db.execSQL(CREATE_TABLE_CDDK);
		}

		/**
		 * Called when database needs an update.
		 */
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CD_MOBILE);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHIENDICH);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHIENDICH_DK);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_STEP);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGE);
			SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(mContext);
			mPref.edit().putBoolean("isFirst", true);
			mPref.edit().commit();
			onCreate(db);
		}
	}
}
