package com.vn.tutela.database;

public class OutOfStorageSpaceException extends StorageUnavailableException {
	public OutOfStorageSpaceException(String description) {
		super(description, "");
	}
}
