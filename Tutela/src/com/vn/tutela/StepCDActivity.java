package com.vn.tutela;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.util.ArrayList;

import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.vn.tutela.base.BaseActivity;
import com.vn.tutela.models.ChienDichStep;
import com.vn.tutela.util.Constants;
import com.vn.tutela.util.CustomMultiPartEntity;
import com.vn.tutela.util.CustomMultiPartEntity.ProgressListener;
import com.vn.tutela.util.IntentUtil;
import com.vn.tutela.util.NetworkUtil;
import com.vn.tutela.util.TimeTextView;
import com.vn.tutela.util.TutelaScrollView;
import com.vn.tutela.util.TutelaScrollView.ScrollListener;
import com.vn.tutela.util.TutelaTextView;
import com.vn.tutela.volley.VolleySingleton;

public class StepCDActivity extends BaseActivity implements OnClickListener,
		ScrollListener {

	TutelaScrollView scroll_Step;
	ImageView titleStep1, titleStep2, titleStep3, titleStep4, bannerStep;
	TimeTextView contentStep1, contentStep2, contentStep3;
	int height;
	boolean isFirst = true;
	private int idCDDK;
	private boolean isOnline;
	private ArrayList<ChienDichStep> thisStep;
	String mImageCache;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		idCDDK = getIntent().getIntExtra(Constants.EXTRA_IDCDDK, 0);
		isOnline = getIntent().getBooleanExtra(Constants.EXTRA_ISONLINE, false);
		Log.d("Data intent", "" + idCDDK + " " + isOnline);
		if (mApp.getListCDDK().get(idCDDK).getDate_end() * 1000l <= System
				.currentTimeMillis()) {
			setContentView(R.layout.layout_thank_cd_end);
			overridePendingTransition(R.anim.trans_left_in,
					R.anim.trans_left_out);
			return;
		}
		setContentView(R.layout.activity_step_cd);
		overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		height = displaymetrics.heightPixels;
		initViews();
		initModels();
	}

	private void initViews() {
		scroll_Step = (TutelaScrollView) findViewById(R.id.sv_step);
		scroll_Step.setOnScrollListener(this);
		findViewById(R.id.buoc_1).setOnClickListener(this);
		findViewById(R.id.buoc_2).setOnClickListener(this);
		findViewById(R.id.buoc_3).setOnClickListener(this);
		findViewById(R.id.buoc_4).setOnClickListener(this);
		findViewById(R.id.bt_take_photo).setOnClickListener(this);
		findViewById(R.id.bt_select_img).setOnClickListener(this);
		titleStep1 = (ImageView) findViewById(R.id.iv_title_step_1);
		titleStep2 = (ImageView) findViewById(R.id.iv_title_step_2);
		titleStep3 = (ImageView) findViewById(R.id.iv_title_step_3);
		titleStep4 = (ImageView) findViewById(R.id.iv_title_step_4);

		contentStep1 = (TimeTextView) findViewById(R.id.tv_content_step_1);
		contentStep2 = (TimeTextView) findViewById(R.id.tv_content_step_2);
		contentStep3 = (TimeTextView) findViewById(R.id.tv_content_step_3);

		bannerStep = (ImageView) findViewById(R.id.iv_step_cd);

		ViewGroup.LayoutParams layout = findViewById(R.id.ll_add)
				.getLayoutParams();
		layout.height = height;
		findViewById(R.id.ll_add).requestLayout();
	}

	private void initModels() {
		thisStep = mApp.getListCDDK().get(idCDDK).getList_step();
		Log.d("DAta Step", mApp.getListCDDK().get(idCDDK).toString() + "  "
				+ thisStep.size());
		if (thisStep != null && thisStep.size() > 0) {
			for (int i = 0; i < thisStep.size(); i++) {
				Log.d("Step", thisStep.get(i).toString());
				setData(thisStep.get(i));
			}
		} else {
			Log.d("data", "NOT STEP");
		}
		File imageFile = new File(Constants.FOLDER_IMAGE + "/"
				+ mApp.getListCDDK().get(idCDDK).getBanner());
		if (imageFile.exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(imageFile
					.getAbsolutePath());
			if (bitmap != null) {
				bannerStep.setImageBitmap(bitmap);
				ViewGroup.LayoutParams layout = findViewById(R.id.ll_add)
						.getLayoutParams();
				layout.height = height - findViewById(R.id.step_4).getHeight()
						- findViewById(R.id.thanh_remote).getHeight();
				findViewById(R.id.ll_add).requestLayout();
			}
		} else if (isNetwork()) {
			ImageRequest request = new ImageRequest(Constants.URL_PRE_IMG
					+ mApp.getListCDDK().get(idCDDK).getBanner(),
					new Response.Listener() {

						@Override
						public void onResponse(Object bitmap) {
							Bitmap bit = (Bitmap) bitmap;
							bannerStep.setImageBitmap((Bitmap) bitmap);

							ViewGroup.LayoutParams layout = findViewById(
									R.id.ll_add).getLayoutParams();
							layout.height = height
									- findViewById(R.id.step_4).getHeight()
									- findViewById(R.id.thanh_remote)
											.getHeight();
							findViewById(R.id.ll_add).requestLayout();
							try {
								saveBitmap((Bitmap) bitmap, mApp.getListCDDK()
										.get(idCDDK).getBanner());
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}
						}
					}, 0, 0, null, new Response.ErrorListener() {
						public void onErrorResponse(VolleyError error) {
							// story_image.setImageResource(R.drawable.image_load_error);
						}
					});
			// Access the RequestQueue through your singleton class.
			VolleySingleton.getInstance().getRequestQueue().add(request);
		}

	}

	private void setData(ChienDichStep step) {
		if (step.getStep_name().equals(Constants.STEP_KNOW))
			contentStep1.setText(Html.fromHtml(step.getLeft() + "<br>"
					+ step.getRight()));
		if (step.getStep_name().equals(Constants.STEP_PLAN))
			contentStep2.setText(Html.fromHtml(step.getLeft() + "<br>"
					+ step.getRight()));
		if (step.getStep_name().equals(Constants.STEP_DO))
			contentStep3.setText(Html.fromHtml(step.getLeft() + "<br>"
					+ step.getRight()));
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buoc_1:
			focusOnView(1);
			break;
		case R.id.buoc_2:
			focusOnView(2);
			break;
		case R.id.buoc_3:
			focusOnView(3);
			break;
		case R.id.buoc_4:
			focusOnView(4);
			break;
		case R.id.bt_take_photo:
			if (!isNetwork())
				break;
			File folder = new File(Constants.FOLDER_IMG_CACHE);
			if (!folder.exists())
				folder.mkdirs();
			mImageCache = Constants.FOLDER_IMG_CACHE + File.separator
					+ "tutela_take_photo.png";
			IntentUtil.captureImage(StepCDActivity.this, mImageCache);
			break;
		case R.id.bt_select_img:
			if (!isNetwork())
				break;
			IntentUtil.getImageFromGallery(StepCDActivity.this);
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case IntentUtil.CAPTURE_IMAGE:
			if (resultCode == RESULT_OK) {
				String filePath;
				if (mImageCache != null) {
					Log.d("File Upload", mImageCache);
					filePath = mImageCache;
					// new UploadAsyncTask().execute(mImageCache);
				} else {
					filePath = Constants.FOLDER_IMG_CACHE + File.separator
							+ "tutela_take_photo.png";
					// new UploadAsyncTask().execute(Constants.FOLDER_IMG_CACHE
					// + File.separator
					// + "tutela_take_photo.png");
				}

				Intent i = new Intent(StepCDActivity.this, UploadActivity.class);
				i.putExtra(Constants.EXTRA_IDCDDK, idCDDK);
				i.putExtra(Constants.EXTRA_FILE_PATH, filePath);
				startActivity(i);

			}
			break;
		case IntentUtil.GET_IMAGE_FROM_GALLERY:
			if (resultCode == RESULT_OK) {
				Uri contentUri = data.getData();
				String filePath = IntentUtil.getRealPathFromURI(
						StepCDActivity.this, contentUri);
				// new UploadAsyncTask().execute(filePath);
				Intent i = new Intent(StepCDActivity.this, UploadActivity.class);
				i.putExtra(Constants.EXTRA_IDCDDK, idCDDK);
				i.putExtra(Constants.EXTRA_FILE_PATH, filePath);
				startActivity(i);
			}
			break;
		default:
			break;
		}
	}

	private class UploadAsyncTask extends AsyncTask<String, Integer, String> {

		private String filePath;
		ProgressDialog pd;
		long totalSize;

		@Override
		protected void onPreExecute() {
			pd = new ProgressDialog(StepCDActivity.this,
					R.style.TransparentProgressDialog);
			pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pd.setProgressDrawable(getResources().getDrawable(
					R.drawable.custom_progress));
			pd.setMessage(getString(R.string.upload_img));
			pd.setButton(getString(R.string.huy),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							// arg0.dismiss();
							cancel(true);
						}
					});
			pd.setCancelable(false);
			pd.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			filePath = params[0];
			CustomMultiPartEntity entity = new CustomMultiPartEntity(
					new ProgressListener() {

						@Override
						public void transferred(long num) {
							publishProgress((int) ((num / (float) totalSize) * 100));
						}
					});
			Charset chars = Charset.forName("UTF-8");
			try {
				File file = new File(filePath);
				if (!file.exists() || file == null)
					return "{succes :false}";
				ContentBody fileContent = new FileBody(file);
				entity.addPart("image", fileContent);
				entity.addPart("user_id",
						new StringBody(mApp.currentUser.getUser_id() + ""));
				entity.addPart("fb_id",
						new StringBody(mApp.currentUser.getFb_id() + ""));
				entity.addPart("id_chiendich", new StringBody(mApp
						.getListCDDK().get(idCDDK).getId()
						+ ""));
				totalSize = entity.getContentLength();
				return NetworkUtil.postInfoWithFile(Constants.API_UPLOAD_ANH,
						null, entity, 10 * 1000);
			} catch (Exception e) {
				return "{succes :false}";
			}
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			super.onProgressUpdate(progress);
			pd.setProgress((int) (progress[0]));
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.d("result", result);
			try {
				JSONObject re = new JSONObject(result);
				if (!re.isNull("success") && re.getBoolean("success")) {
					mApp.getListCDDK().get(idCDDK).getUser_cp().setStt(0);
					showToast(getString(R.string.success_upload));
				} else {
					if (!re.isNull("message"))
						showToast(re.getString("message"));
					else
						showToast(getString(R.string.fail_upload));
				}
			} catch (JSONException e) {
				e.printStackTrace();
				showToast(getString(R.string.fail_upload));
			}

			pd.dismiss();
		}

	}

	private final void focusOnView(int i) {
		switch (i) {
		case 1:
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					scroll_Step.scrollTo(0, findViewById(R.id.iv_step_cd)
							.getHeight());
				}
			});
			break;
		case 2:
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					scroll_Step.scrollTo(0, findViewById(R.id.iv_step_cd)
							.getHeight()
							+ titleStep1.getHeight()
							+ findViewById(R.id.tv_content_step_1).getHeight());
				}
			});
			break;
		case 3:
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					scroll_Step.scrollTo(
							0,
							findViewById(R.id.iv_step_cd).getHeight()
									+ titleStep1.getHeight()
									+ findViewById(R.id.tv_content_step_1)
											.getHeight()
									+ titleStep2.getHeight()
									+ findViewById(R.id.tv_content_step_2)
											.getHeight());
				}
			});
			break;
		case 4:
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					scroll_Step.scrollTo(
							0,
							findViewById(R.id.iv_step_cd).getHeight()
									+ titleStep1.getHeight()
									+ findViewById(R.id.tv_content_step_1)
											.getHeight()
									+ titleStep2.getHeight()
									+ findViewById(R.id.tv_content_step_2)
											.getHeight()
									+ titleStep3.getHeight()
									+ findViewById(R.id.tv_content_step_3)
											.getHeight());
				}
			});
			break;
		default:
			break;
		}
	}

	@Override
	public void onScrollChanged(int t) {
		if (isFirst) {
			ViewGroup.LayoutParams layout = findViewById(R.id.ll_add)
					.getLayoutParams();
			layout.height = this.height - findViewById(R.id.step_4).getHeight()
					- findViewById(R.id.thanh_remote).getHeight();
			findViewById(R.id.ll_add).requestLayout();
			isFirst = false;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
	}

}
