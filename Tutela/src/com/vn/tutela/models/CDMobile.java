package com.vn.tutela.models;

/**
 * @author Vinh
 *
 */
public class CDMobile {
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id_cdm
	 * @var integer $id_chiendich
	 * @var text $img_adv
	 * @var text $img_regis
	 * @var text $img_list
	 */
	
	private int id_cdm;
	private int id_chiendich;
	private String img_adv;
	private String img_regis;
	private String img_list;
	
	public CDMobile() {
		super();
		this.id_cdm = 0;
		this.id_chiendich = 0;
		this.img_adv = "";
		this.img_regis = "";
		this.img_list = "";
	} 
	
	public int getId_cdm() {
		return id_cdm;
	}
	public void setId_cdm(int id_cdm) {
		this.id_cdm = id_cdm;
	}
	public int getId_chiendich() {
		return id_chiendich;
	}
	public void setId_chiendich(int id_chiendich) {
		this.id_chiendich = id_chiendich;
	}
	public String getImg_adv() {
		return img_adv;
	}
	public void setImg_adv(String img_adv) {
		this.img_adv = img_adv;
	}
	public String getImg_regis() {
		return img_regis;
	}
	public void setImg_regis(String img_regis) {
		this.img_regis = img_regis;
	}
	public String getImg_list() {
		return img_list;
	}
	public void setImg_list(String img_list) {
		this.img_list = img_list;
	}
	public CDMobile(int id_cdm, int id_chiendich, String img_adv,
			String img_regis, String img_list) {
		super();
		this.id_cdm = id_cdm;
		this.id_chiendich = id_chiendich;
		this.img_adv = img_adv;
		this.img_regis = img_regis;
		this.img_list = img_list;
	} 
	
}
