package com.vn.tutela.models;

import java.util.ArrayList;

/**
 * @author Vinh
 *
 */

public class ChienDich {
	/**
	 * The followings are the available columns in table 'chiendich':
	 * @var integer $id
	 * @var integer $create
	 * @var string $title
	 * @var string $sort_des
	 * @var integer $date_create
	 * @var integer $date_end
	 * @var string $image
	 * @var string $banner
	 * @var integer $id_kind
	 * @var integer $id_time
	 * @var integer $id_type
	 * @var integer $kind_step
	 */
	private int id;
	private int id_dk;
	private long create;
	private String title;
	private String sort_des;
	private long date_create;
	private long date_end;
	private String image;
	private String banner;
	private int id_kind;
	private int id_time;
	private int id_type;
	private int kind_step;
	private ArrayList<ChienDichStep> list_step;
	private UserCP user_cp;
	private String image_upload;
	
	public UserCP getUser_cp() {
		return user_cp;
	}

	public void setUser_cp(UserCP user_cp) {
		this.user_cp = user_cp;
	}

	public ChienDich(){
		this.id = 0;
		this.id_dk = 0;
		this.create = 0;
		this.title = "";
		this.sort_des = "";
		this.date_create = System.currentTimeMillis();
		this.date_end = System.currentTimeMillis();
		this.image = "";
		this.banner = "";
		this.id_kind = 0;
		this.id_time = 0;
		this.id_type = 0;
		this.kind_step = 0;
		this.list_step = new ArrayList<ChienDichStep>();
		this.user_cp = new UserCP();
	}
	
	public ChienDich(int id, long create, String title, String sort_des,
			long date_create, long date_end, String image, String banner,
			int id_kind, int id_time, int id_type, int kind_step,
			ArrayList<ChienDichStep> list_step) {
		super();
		this.id = id;
		this.create = create;
		this.title = title;
		this.sort_des = sort_des;
		this.date_create = date_create;
		this.date_end = date_end;
		this.image = image;
		this.banner = banner;
		this.id_kind = id_kind;
		this.id_time = id_time;
		this.id_type = id_type;
		this.kind_step = kind_step;
		this.list_step = list_step;
	}
	public ArrayList<ChienDichStep> getList_step() {
		return list_step;
	}
	public void setList_step(ArrayList<ChienDichStep> list_step) {
		this.list_step = list_step;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getCreate() {
		return create;
	}
	public void setCreate(long create) {
		this.create = create;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSort_des() {
		return sort_des;
	}
	public void setSort_des(String sort_des) {
		this.sort_des = sort_des;
	}
	public long getDate_create() {
		return date_create;
	}
	public void setDate_create(long date_create) {
		this.date_create = date_create;
	}
	public long getDate_end() {
		return date_end;
	}
	public void setDate_end(long date_end) {
		this.date_end = date_end;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getBanner() {
		return banner;
	}
	public void setBanner(String banner) {
		this.banner = banner;
	}
	public int getId_kind() {
		return id_kind;
	}
	public void setId_kind(int id_kind) {
		this.id_kind = id_kind;
	}
	public int getId_time() {
		return id_time;
	}
	public void setId_time(int id_time) {
		this.id_time = id_time;
	}
	public int getId_type() {
		return id_type;
	}
	public void setId_type(int id_type) {
		this.id_type = id_type;
	}
	public int getKind_step() {
		return kind_step;
	}
	public void setKind_step(int kind_step) {
		this.kind_step = kind_step;
	}

	@Override
	public String toString() {
		return "ChienDich [id=" + id + ", create=" + create + ", title="
				+ title + ", sort_des=" + sort_des + ", date_create="
				+ date_create + ", date_end=" + date_end + ", image=" + image
				+ ", banner=" + banner + ", id_kind=" + id_kind + ", id_time="
				+ id_time + ", id_type=" + id_type + ", kind_step=" + kind_step
				+ "]";
	}

	public int getId_dk() {
		return id_dk;
	}

	public void setId_dk(int id_dk) {
		this.id_dk = id_dk;
	}
	
}
