package com.vn.tutela.models;

/**
 * @author Vinh
 *
 */
public class Time {
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id_time
	 * @var string $text
	 * @var integer $stt
	 */
	
	private int id_time;
	private String text;
	private int stt;
	
	public Time() {
		super();
		this.id_time = 0;
		this.text = "";
		this.stt = 0;
	}
	
	public int getId_time() {
		return id_time;
	}
	public void setId_time(int id_time) {
		this.id_time = id_time;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getStt() {
		return stt;
	}
	public void setStt(int stt) {
		this.stt = stt;
	}
	public Time(int id_time, String text, int stt) {
		super();
		this.id_time = id_time;
		this.text = text;
		this.stt = stt;
	}
	
}
