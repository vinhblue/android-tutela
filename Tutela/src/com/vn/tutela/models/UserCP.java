package com.vn.tutela.models;

/**
 * @author Vinh
 *
 */
public class UserCP {
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id
	 * @var integer $user_id
	 * @var integer $id_chiendich
	 * @var integer $stt
	 * @var integer $isWeb
	 */
	
	private int id;
	private long user_id;
	private int stt;
	private int isWeb;
	private String title;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getId() {
		return id;
	}
	
	public UserCP() {
		super();
		this.id = 0;
		this.user_id = 0;
		this.stt = 0;
		this.isWeb = 0;
		this.title = "";
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public int getStt() {
		return stt;
	}
	public void setStt(int stt) {
		this.stt = stt;
	}
	public int getIsWeb() {
		return isWeb;
	}
	public void setIsWeb(int isWeb) {
		this.isWeb = isWeb;
	}
	
}
