package com.vn.tutela.models;

/**
 * @author Vinh
 *
 */
public class ThoStory {
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id
	 * @var string $c_name
	 * @var string $char_list
	 * @var string $c_image1
	 * @var string $c_image2
	 * @var string $c_image3
	 * @var string $c_help
	 * @var string $tho_image
	 */
	
	private int id;
	private String c_name;
	private String char_list;
	private String c_image1;
	private String c_image2;
	private String c_image3;
	private String c_help;
	
	public ThoStory() {
		super();
		this.id = 0;
		this.c_name = "";
		this.char_list = "";
		this.c_image1 = "";
		this.c_image2 = "";
		this.c_image3 = "";
		this.c_help = "";
		this.tho_image = "";
	}
	
	public ThoStory(int id, String c_name, String char_list, String c_image1,
			String c_image2, String c_image3, String c_help, String tho_image) {
		super();
		this.id = id;
		this.c_name = c_name;
		this.char_list = char_list;
		this.c_image1 = c_image1;
		this.c_image2 = c_image2;
		this.c_image3 = c_image3;
		this.c_help = c_help;
		this.tho_image = tho_image;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public String getChar_list() {
		return char_list;
	}
	public void setChar_list(String char_list) {
		this.char_list = char_list;
	}
	public String getC_image1() {
		return c_image1;
	}
	public void setC_image1(String c_image1) {
		this.c_image1 = c_image1;
	}
	public String getC_image2() {
		return c_image2;
	}
	public void setC_image2(String c_image2) {
		this.c_image2 = c_image2;
	}
	public String getC_image3() {
		return c_image3;
	}
	public void setC_image3(String c_image3) {
		this.c_image3 = c_image3;
	}
	public String getC_help() {
		return c_help;
	}
	public void setC_help(String c_help) {
		this.c_help = c_help;
	}
	public String getTho_image() {
		return tho_image;
	}
	public void setTho_image(String tho_image) {
		this.tho_image = tho_image;
	}
	private String tho_image;
}
