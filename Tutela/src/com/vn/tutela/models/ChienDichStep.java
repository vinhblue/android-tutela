package com.vn.tutela.models;

/**
 * @author Vinh
 *
 */
public class ChienDichStep {
	@Override
	public String toString() {
		return "ChienDichStep [id_step=" + id_step + ", id_chiendich="
				+ id_chiendich + ", right=" + right + ", left=" + left
				+ ", step_name=" + step_name + "]";
	}
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id_step
	 * @var integer $id_chiendich
	 * @var string $right
	 * @var string $left
	 * @var string $step_name
	 */
	
	private int id_step;
	private int id_chiendich;
	private String right;
	private String left;
	private String step_name;
	public ChienDichStep() {
		this.id_step = 0;
		this.id_chiendich = 0;
		this.right = "";
		this.left = "";
		this.step_name = "";
	}
	public ChienDichStep(int id_step, int id_chiendich, String right,
			String left, String step_name) {
		super();
		this.id_step = id_step;
		this.id_chiendich = id_chiendich;
		this.right = right;
		this.left = left;
		this.step_name = step_name;
	}
	public int getId_step() {
		return id_step;
	}
	public void setId_step(int id_step) {
		this.id_step = id_step;
	}
	public int getId_chiendich() {
		return id_chiendich;
	}
	public void setId_chiendich(int id_chiendich) {
		this.id_chiendich = id_chiendich;
	}
	public String getRight() {
		return right;
	}
	public void setRight(String right) {
		this.right = right;
	}
	public String getLeft() {
		return left;
	}
	public void setLeft(String left) {
		this.left = left;
	}
	public String getStep_name() {
		return step_name;
	}
	public void setStep_name(String step_name) {
		this.step_name = step_name;
	}
}
