package com.vn.tutela.models;

/**
 * @author Vinh
 *
 */
public class Story {
	@Override
	public String toString() {
		return "Story [id_story=" + id_story + ", name=" + name + ", info1="
				+ info1 + ", info2=" + info2 + ", images=" + images + "]";
	}
	/**
	 * The followings are the available columns in table 'tbl_user':
	 * @var integer $id_story
	 * @var string $name
	 * @var string $info1
	 * @var string $info2
	 * @var string $images
	 */
	
	private int id_story;
	private String name;
	private String info1;
	private String info2;
	private String images;
	
	public Story() {
		super();
		this.id_story = 0;
		this.name = "";
		this.info1 = "";
		this.info2 = "";
		this.images = "";
	}
	
	public Story(int id_story, String name, String info1, String info2,
			String images) {
		super();
		this.id_story = id_story;
		this.name = name;
		this.info1 = info1;
		this.info2 = info2;
		this.images = images;
	}
	public int getId_story() {
		return id_story;
	}
	public void setId_story(int id_story) {
		this.id_story = id_story;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInfo1() {
		return info1;
	}
	public void setInfo1(String info1) {
		this.info1 = info1;
	}
	public String getInfo2() {
		return info2;
	}
	public void setInfo2(String info2) {
		this.info2 = info2;
	}
	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}
}
