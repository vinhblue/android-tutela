package com.vn.tutela.models;

/**
 * @author Vinh
 *
 */
public class User {
	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", fb_id=" + fb_id
				+ ", first_name=" + first_name + ", last_name=" + last_name
				+ ", email=" + email + ", role=" + role + ", date_reg="
				+ date_reg + " birthday= " + birthday + "]";
	}

	private int user_id;
	private long fb_id;
	private String first_name;
	private String last_name;
	private String email;
	private int role;
	private long date_reg;
	private int birthday;
	private String city;
	
	public int getBirthday() {
		return birthday;
	}

	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}

	public User(){
		this.user_id = 0;
		this.fb_id = 0;
		this.first_name = "";
		this.last_name = "";
		this.role =0;
		this.birthday = 0;
		this.city = "";
		this.date_reg = System.currentTimeMillis();
	}
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public long getFb_id() {
		return fb_id;
	}
	public void setFb_id(long fb_id) {
		this.fb_id = fb_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	public long getDate_reg() {
		return date_reg;
	}
	public void setDate_reg(long date_reg) {
		this.date_reg = date_reg;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
