package com.vn.tutela;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.vn.tutela.base.BaseActivity;

public class SplashActivity extends BaseActivity
{
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(R.anim.trans_bottom_in, R.anim.trans_bottom_out);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_splash);
    overridePendingTransition(R.anim.trans_top_in, R.anim.trans_top_out);
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        Intent localIntent = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(localIntent);
        finish();
      }
    }
    , 2000L);
  }
}