package com.vn.tutela.adapter;

import java.io.File;
import java.util.ArrayList;

import com.vn.tutela.R;
import com.vn.tutela.models.ChienDich;
import com.vn.tutela.models.UserCP;
import com.vn.tutela.util.BitmapUtil;
import com.vn.tutela.util.Constants;
import com.vn.tutela.util.TutelaTextView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author Vinh
 *
 */
public class AdapterListChiendichdk extends BaseAdapter{
	
	private Context mContext;
	private ArrayList<ChienDich> data;
	
	public AdapterListChiendichdk(Context context, ArrayList<ChienDich> listcd){
		this.mContext = context;
		this.data = listcd;
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(this.data == null)
			return 0;
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		if(this.data == null)
		return null;
		else return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int p, View view, ViewGroup vg) {
		ChienDichHolder holder;
		if(view == null){
			holder = new ChienDichHolder();
			LayoutInflater inflater = LayoutInflater.from(mContext);
			view = inflater.inflate(R.layout.item_cd_dadk, null);
			holder.rlBg = (RelativeLayout)view.findViewById(R.id.bg_itemcd);
			holder.ivImgCD = (ImageView) view.findViewById(R.id.img_cd);
			holder.tvNameCD = (TutelaTextView) view.findViewById(R.id.tv_title_cd);
			view.setTag(holder);
		}else{
			holder = (ChienDichHolder)view.getTag();
		}
		ChienDich cd = data.get(p);
		if(cd.getUser_cp().getStt()== 0){
			holder.rlBg.setBackgroundResource(R.drawable.bg_cd_da_thuchien);
		}
		holder.tvNameCD.setText(cd.getTitle());
		String filePath = Constants.FOLDER_IMG_UPLOAD + File.separator
				+ cd.getId() +".png";
		File imageFile = new File(filePath);
		Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
		if (bitmap != null) {
			holder.ivImgCD.setImageBitmap(BitmapUtil.getCircleBitmap(bitmap, Constants.SIZE_IMAGE_UPLOAD_SAVE, Color.parseColor("#30ffffff")));
		}
		
		
		return view;
	}
	
	public class ChienDichHolder{
		RelativeLayout rlBg;
		ImageView ivImgCD;
		TutelaTextView tvNameCD;
	}

}
