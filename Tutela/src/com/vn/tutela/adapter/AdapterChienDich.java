package com.vn.tutela.adapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.vn.tutela.R;
import com.vn.tutela.fragments.ChienDichFragment;
import com.vn.tutela.fragments.ChienDichFragment.FragmentOnClick;
import com.vn.tutela.models.CDMobile;
import com.vn.tutela.util.Constants;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class AdapterChienDich extends FragmentPagerAdapter{
	
	private ArrayList<CDMobile> data;
	private FragmentOnClick fgClickListener;
	private boolean isOnline;
	
	public AdapterChienDich(FragmentManager fm, ArrayList<CDMobile> mdata, FragmentOnClick fgClickListener,boolean isOnline) {
		super(fm);
		this.data = mdata;
		this.fgClickListener = fgClickListener;
		this.isOnline = isOnline;
	}

	
	@Override
	public Fragment getItem(int p) {
		return ChienDichFragment.create(p,this.fgClickListener,isOnline);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}
	
}
