package com.vn.tutela.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TutelaTextView extends TextView {

	public TutelaTextView(Context context) {
		super(context);
	}

	public TutelaTextView(Context context, AttributeSet attributeset) {
		super(context, attributeset);
	}

	public TutelaTextView(Context context, AttributeSet attributeset, int i) {
		super(context, attributeset, i);
	}

	@Override
	public void setTypeface(Typeface tf, int style) {
		if (!isInEditMode()) {
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/UTM_Cookies.ttf"));
		}
	}

}
