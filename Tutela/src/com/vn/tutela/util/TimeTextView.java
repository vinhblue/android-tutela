package com.vn.tutela.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TimeTextView extends TextView {

	public TimeTextView(Context context) {
		super(context);
	}

	public TimeTextView(Context context, AttributeSet attributeset) {
		super(context, attributeset);
	}

	public TimeTextView(Context context, AttributeSet attributeset, int i) {
		super(context, attributeset, i);
	}

	@Override
	public void setTypeface(Typeface tf, int style) {
		if (!isInEditMode()) {
			super.setTypeface(Typeface.createFromAsset(
					getContext().getAssets(), "fonts/TIMES.TTF"));
			super.setTextSize(18.0F);
		}
	}

}