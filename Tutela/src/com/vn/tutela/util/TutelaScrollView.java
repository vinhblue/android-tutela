package com.vn.tutela.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class TutelaScrollView extends ScrollView{

	public interface ScrollListener{
		public void onScrollChanged(int t);
	}
	
	public TutelaScrollView(Context context) {
		super(context);
	}
	
	public TutelaScrollView(Context context, AttributeSet attributeset) {
		super(context, attributeset);
	}

	public TutelaScrollView(Context context, AttributeSet attributeset, int i) {
		super(context, attributeset, i);
	}
	
	private ScrollListener listener;
	
	public void setOnScrollListener(ScrollListener listener){
		this.listener = listener;
	}
	
	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		if(this.listener != null)
			this.listener.onScrollChanged(t);
	}

}
