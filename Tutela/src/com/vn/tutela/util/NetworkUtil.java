package com.vn.tutela.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

public class NetworkUtil {
	public static String TAG = NetworkUtil.class.getName();
	
	public static String postInfoWithFile(String url, List<NameValuePair> params,
			HttpEntity httpEntity, int timeout) throws IOException {
		HttpPost httpPost = new HttpPost(url);
		// HttpParams httpParameters = new BasicHttpParams();
		// HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
		// // Set the default socket timeout (SO_TIMEOUT)
		// // in milliseconds which is the timeout for waiting for data.
		// int timeoutSocket = 5000;
		// HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		// httpPost.setParams(httpParameters);
		if (params != null && !params.isEmpty()) {
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		if (httpEntity != null) {
			httpPost.setEntity(httpEntity);
		}
		Log.i(TAG, "postInfoWithFile - done setEntity");
		return getResultFromService(httpPost);
	}

	// Thanhnx
	private static String getResultFromService(HttpRequestBase httpRequestBase)
			throws IOException {

		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
		HttpConnectionParams.setSoTimeout(httpParameters, 30000);
		HttpClient httpclient = new DefaultHttpClient(httpParameters);

		HttpResponse response;
		String result = null;
		try {
			Log.i(TAG, "getResultFromService - start exec");
			response = httpclient.execute(httpRequestBase);
			Log.i(TAG, "getResultFromService - start get content");
			InputStream instream = response.getEntity().getContent();
			Log.i(TAG, "getResultFromService - start convert stream to string");
			result = convertStreamToString(instream);
			instream.close();
		} catch (ConnectTimeoutException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	private static String convertStreamToString(InputStream instream) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				instream));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				// Log.e("convertStreamToString", "line");
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				instream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
