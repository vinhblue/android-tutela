package com.vn.tutela.util;

import java.io.File;

public class Constants {
	public static final String URL_API = "http://api.tutela.vn/site/";
	public static final String URL_IMG = "http://tutela.vn/asset/images/mobile/";
	public static final String URL_PRE_IMG = "http://tutela.vn/asset/images/";
	public static final String URL_IMG_UPLOAD = "http://tutela.vn/asset/images/mobile/";
	public static final String API_GETSTORY = URL_API + "getstory";
	public static final String API_LOGIN = URL_API + "login";
	public static final String API_CDDK = URL_API + "getcddk";
	public static final String API_CDMOBILE = URL_API + "getcd";
	public static final String API_DANGKY = URL_API + "dkcd";
	public static final String API_UPLOAD_ANH = URL_API + "uploadfile";
	
	public static final String FOLDER_ROOT_NAME = "tutela";
	public static final String FOLDER_ROOT_PATH = FileManager.EXTERNAL_PATH
			+ File.separator + FOLDER_ROOT_NAME;
	public static final String FOLDER_IMAGE = FOLDER_ROOT_PATH + File.separator + "images/mobile";
	public static final String FOLDER_IMG_CACHE = FOLDER_ROOT_PATH + File.separator + "images/cache";
	public static final String FOLDER_IMG_UPLOAD = FOLDER_ROOT_PATH + File.separator + "images/upload";
	
	public static final String EXTRA_IDMOBILE = "ID_CDMOBILE";
	public static final String EXTRA_ISONLINE = "IS_ONLINE";
	public static final String EXTRA_IDCDDK = "ID_CDDK";
	public static final String EXTRA_FILE_PATH = "FILE_PATH";
	
	public static final String STEP_KNOW = "know it";
	public static final String STEP_PLAN = "plan it";
	public static final String STEP_DO = "do it";
	
	public static final int SIZE_IMAGE_UPLOAD_SAVE = 169;
	
}
