package com.vn.tutela.fragments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.vn.tutela.R;
import com.vn.tutela.TutelaApplication;
import com.vn.tutela.util.Constants;
import com.vn.tutela.volley.VolleySingleton;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ChienDichFragment extends Fragment {

	public interface FragmentOnClick {
		public void onClick(int id, boolean isOnline);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.mActivity = activity;
	}

	int idBg;

	private Activity mActivity;
	private FragmentOnClick FgClickListener;
	private boolean isOnline;
	ImageView image;

	public ChienDichFragment() {

	}

	public static ChienDichFragment create(int idbg,
			FragmentOnClick fgClickListener, boolean isOnline) {
		ChienDichFragment fragment = new ChienDichFragment();
		fragment.idBg = idbg;
		fragment.FgClickListener = fgClickListener;
		fragment.isOnline = isOnline;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.fragment_chiendich, container, false);
		final TutelaApplication mApp = (TutelaApplication) getActivity()
				.getApplication();
		rootView.findViewById(R.id.ll_root_chiendich).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						FgClickListener.onClick(idBg, isOnline);
					}
				});
		image = (ImageView) rootView.findViewById(R.id.cd_bg);
		File imageFile = new File(Constants.FOLDER_IMAGE + "/"
				+ mApp.getListCDmobile().get(idBg).getImg_adv());
		if (imageFile.exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(imageFile
					.getAbsolutePath());
			if (bitmap != null)
				image.setImageBitmap(bitmap);
		} else if (isNetwork()) {
			ImageRequest request = new ImageRequest(Constants.URL_IMG
					+ mApp.getListCDmobile().get(idBg).getImg_adv(),
					new Response.Listener() {

						@Override
						public void onResponse(Object bitmap) {
							image.setImageBitmap((Bitmap) bitmap);
							try {
								saveBitmap((Bitmap) bitmap, mApp
										.getListCDmobile().get(idBg)
										.getImg_adv());
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							}
						}
					}, 0, 0, null, new Response.ErrorListener() {
						public void onErrorResponse(VolleyError error) {
							// story_image.setImageResource(R.drawable.image_load_error);
						}
					});
			// Access the RequestQueue through your singleton class.
			VolleySingleton.getInstance().getRequestQueue().add(request);
		}
		return rootView;
	}

	protected Uri saveBitmap(Bitmap bitmap, String name)
			throws FileNotFoundException {
		File folder = new File(Constants.FOLDER_IMAGE);
		if (!folder.exists())
			folder.mkdirs();
		File imageFile = new File(Constants.FOLDER_IMAGE, name);
		FileOutputStream fileOutPutStream = null;
		try {
			fileOutPutStream = new FileOutputStream(imageFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutPutStream);

			fileOutPutStream.flush();
			fileOutPutStream.close();
			return Uri.parse("file://" + imageFile.getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	protected boolean isNetwork() {
		ConnectivityManager connectivity = (ConnectivityManager) getActivity()
				.getApplicationContext().getSystemService(
						Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
		}
		return false;
	}
}
