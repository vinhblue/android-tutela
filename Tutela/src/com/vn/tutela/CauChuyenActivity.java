package com.vn.tutela;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.vn.tutela.base.BaseActivity;
import com.vn.tutela.models.Story;
import com.vn.tutela.util.Constants;
import com.vn.tutela.util.QuickopenTextView;
import com.vn.tutela.util.TimeTextView;
import com.vn.tutela.util.TutelaTextView;
import com.vn.tutela.volley.VolleySingleton;

public class CauChuyenActivity extends BaseActivity {

	public static String TAG = CauChuyenActivity.class.getName();

	TimeTextView info1, info2;
	QuickopenTextView title;
	ImageView story_image;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cauchuyen);
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
		showProgressDialog(true);
		initView();
		if (mApp.getStory() == null)
			getData();
		else
			createData(false);
	}

	private void initView() {
		title = (QuickopenTextView) findViewById(R.id.title_story);
		info1 = (TimeTextView) findViewById(R.id.info1);
		info2 = (TimeTextView) findViewById(R.id.info2);
		story_image = (ImageView) findViewById(R.id.story_image);
	}

	/**
	 * createData - show data of Cauchuyen
	 * @param boolean isOnline
	 * 
	 * @return void
	 * */
	private void createData(boolean isOnline) {
		title.setText(mApp.getStory().getName());
		info1.setText(mApp.getStory().getInfo1());
		info2.setText(mApp.getStory().getInfo2());

		// Load image Cauchuyen

		File imageFile = new File(Constants.FOLDER_IMAGE + "/"
				+ mApp.getStory().getImages());
		// If image is exists in local -> load from local - else load online
		if (imageFile.exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(imageFile
					.getAbsolutePath());
			if (bitmap != null)
				story_image.setImageBitmap(bitmap);
		} else if (isNetwork()) {
			ImageRequest request = new ImageRequest(Constants.URL_IMG
					+ mApp.getStory().getImages(), new Response.Listener() {

				@Override
				public void onResponse(Object bitmap) {
					story_image.setImageBitmap((Bitmap) bitmap);
					try {
						saveBitmap((Bitmap) bitmap, mApp.getStory().getImages());
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}, 0, 0, null, new Response.ErrorListener() {
				public void onErrorResponse(VolleyError error) {
					// story_image.setImageResource(R.drawable.image_load_error);
				}
			});
			// Access the RequestQueue through your singleton class.
			VolleySingleton.getInstance().getRequestQueue().add(request);
		}
		// End Load image Cauchuyen
		showProgressDialog(false);
	}

	private void getData() {
		if (isNetwork())
			VolleySingleton
					.getInstance()
					.getRequestQueue()
					.add(new JsonObjectRequest(Request.Method.GET,
							Constants.API_GETSTORY + "?user_id="
									+ mApp.currentUser.getUser_id() + "&fb_id="
									+ mApp.currentUser.getFb_id(), null,
							new Listener<JSONObject>() {

								@Override
								public void onResponse(JSONObject data) {
									try {
										Log.d("dataJson", data.toString());
										Story s = insertData(data);
										mApp.setStory(s);
										mApp.getLocalStorage().insertStory(s);
										createData(true);

									} catch (JSONException e) {
										showProgressDialog(false);
										e.printStackTrace();
									}
								}
							}, new Response.ErrorListener() {

								@Override
								public void onErrorResponse(VolleyError arg0) {
									showProgressDialog(false);
								}
							}));
		else
			loadDatabase();
	}

	private void loadDatabase() {
		mApp.setStory(mApp.getLocalStorage().getStory());
		Log.d(TAG, mApp.getStory().toString());
		createData(false);
	}

	private Story insertData(JSONObject data) throws JSONException {
		Story story = new Story();
		if (!data.isNull("success") && data.getBoolean("success")) {
			if (!data.isNull("story")) {
				JSONObject jstory = data.getJSONObject("story");
				if (!jstory.isNull("id_story"))
					story.setId_story(jstory.getInt("id_story"));
				if (!jstory.isNull("name"))
					story.setName(jstory.getString("name"));
				if (!jstory.isNull("id_story"))
					story.setInfo1(jstory.getString("info1"));
				if (!jstory.isNull("info2"))
					story.setInfo2(jstory.getString("info2"));
				if (!jstory.isNull("images"))
					story.setImages(jstory.getString("images"));
			}
		}
		Log.d(TAG, story.toString());
		return story;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
	}
}
